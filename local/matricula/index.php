<?php

include('../../config.php');
require_once $CFG->dirroot . '/vendor/autoload.php';

use matricula\Core\Module;


$context_system = context_system::instance();

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    // Symfony Start.
    Module::start();
    // Symfony End.
} else {
    if (strpos($_SERVER['REQUEST_URI'], 'xlsx') !== false) {
        Module::start();
    } else {
        $PAGE->set_pagelayout('coursecategory');
       $name = get_string('name', 'local_matricula');
        $PAGE->set_context($context_system);
        $PAGE->navbar->add('Softper');
        $PAGE->set_title('Softper');
        $PAGE->set_heading('Softper');
        $url = new \moodle_url($PAGE->url);
        $PAGE->set_url($url);
    $stringman = get_string_manager();
        $strings = $stringman->load_component_strings('local_matricula', $CFG->lang);
        $PAGE->requires->data_for_js('local_matricula', $strings);
        $PAGE->requires->js_call_amd('local_matricula/matricula', 'init');
        $PAGE->requires->js_call_amd('local_matricula/alumnos', 'init');
        $PAGE->requires->js_call_amd('local_matricula/cursos', 'init');
        $PAGE->requires->js_call_amd('local_matricula/docentes', 'init');
        $PAGE->requires->js_call_amd('local_matricula/reportes', 'init');
        $PAGE->requires->css('/local/matricula/Resources/css/spin.css');
        $PAGE->requires->css('/local/matricula/Resources/css/modal.css');
        $PAGE->requires->css('/local/matricula/Resources/css/matricula.css');
        $PAGE->requires->css('/local/matricula/Resources/css/alumnos.css');
        $PAGE->requires->css('/local/matricula/Resources/css/buscador.css');
        $PAGE->requires->css('/local/matricula/Resources/css/select2.min.css');
        $PAGE->requires->css('/local/matricula/Resources/css/select2-bootstrap4.min.css');
        $PAGE->requires->css('/local/matricula/Resources/css/jquery.dataTables.min.css');
        $PAGE->requires->css('/local/matricula/Resources/css/dataTables.bootstrap4.min.css');
        $PAGE->requires->css('/local/matricula/Resources/css/bs-stepper.min.css');
        $PAGE->requires->css('/local/matricula/Resources/css/tooltipster.bundle.min.css');
        $PAGE->requires->css('/local/matricula/Resources/css/fixeheader.dataTables.min.css');
        echo $OUTPUT->header();
        // Symfony Start.
        Module::start();
        // Symfony End.
        echo $OUTPUT->footer();
    }
}