<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Ensure the configurations for this site are set
if ( $hassiteconfig ){
 
	// Create the new settings page
	// - in a local plugin this is not defined as standard, so normal $settings->methods will throw an error as
	// $settings will be NULL
	$settings = new admin_settingpage( 'local_matricula', 'Your Settings Page Title' );
 
	// Create 
	$ADMIN->add('localplugins', new admin_externalpage('local_matricula', get_string('admin', 'local_matricula'), new moodle_url('/local/matricula')));
 
}