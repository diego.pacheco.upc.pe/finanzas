<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Controller;

use matricula\Service\ReporteService;
use matricula\Service\UtilService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use core\output\notification;
use context_system;
use moodle_url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class ReporteController extends ReporteService {

    private $params;

    const ORIGEN_SOLICITUDES = 'solicitudes';
    const ORIGEN_NUEVO_SOLICITUDES = 'nuevo-solicitudes';
    const ORIGEN_ESTADOS_SOLICITUDES = 'estados-solicitudes';
    const ORIGEN_FACTURACION = 'facturacion';
    const ORIGEN_ALUMNOS = 'alumnos';

    public function __construct() {
        $this->params = [];
    }

    /**
     * Vista para listar los registros de la tabla sílabos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $this->params['reportes_cursos'] = $this->routes()->generate('reportes_cursos');
        $this->params['reportes_alumnos'] = $this->routes()->generate('reportes_alumnos');
        $this->params['reportes_docentes'] = $this->routes()->generate('reportes_docentes');
        $this->params['reportes_matriculas'] = $this->routes()->generate('reportes_matriculas');
        $this->params['top_admision'] = $this->getTopAdmision();
        $this->params['top_carreras'] = $this->getTopAlumnos();
        $this->params['top_procedencias'] = $this->getTopProcedencias();
        return $this->template('Reporte')->renderResponse('index.html.twig', $this->params);
    }

    public function cursosAction() {
        $ciclos= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
        $cursos = \matricula\Service\CursoService::getCursosAll();
        $this->params['title'] = 'Reporte de compras por cliente';
        $this->params['url_ajax'] = $this->routes()->generate('reportes_ajax');
        $this->params['reportes_cursos'] = $this->routes()->generate('reportes_cursos');
        $this->params['reportes_alumnos'] = $this->routes()->generate('reportes_alumnos');
        $this->params['reportes_docentes'] = $this->routes()->generate('reportes_docentes');
        $this->params['reportes_matriculas'] = $this->routes()->generate('reportes_matriculas');
        $this->params['cursos'] = $cursos;
        $this->params['ciclos'] = $cursos;
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Reporte')->renderResponse('cursos.html.twig', $this->params);
    }
    
    public function alumnosAction() {
        $this->params['title'] = 'Reporte de alumnos por curso';
        $cursos = \matricula\Service\CursoService::getCursosAll();
        $this->params['cursos'] = $cursos;
        $this->params['url_ajax'] = $this->routes()->generate('reportes_ajax');
        $this->params['reportes_cursos'] = $this->routes()->generate('reportes_cursos');
        $this->params['reportes_alumnos'] = $this->routes()->generate('reportes_alumnos');
        $this->params['reportes_docentes'] = $this->routes()->generate('reportes_docentes');
        $this->params['reportes_matriculas'] = $this->routes()->generate('reportes_matriculas');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Reporte')->renderResponse('alumnos.html.twig', $this->params);
    }
     public function docentesAction() {
        $this->params['title'] = 'Reporte de docentes registrados';
        $this->params['url_ajax'] = $this->routes()->generate('reportes_ajax');
        $this->params['reportes_cursos'] = $this->routes()->generate('reportes_cursos');
        $this->params['reportes_alumnos'] = $this->routes()->generate('reportes_alumnos');
        $this->params['reportes_docentes'] = $this->routes()->generate('reportes_docentes');
        $this->params['reportes_matriculas'] = $this->routes()->generate('reportes_matriculas');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Reporte')->renderResponse('docentes.html.twig', $this->params);
    }
     public function matriculasAction() {
         
        $semestres = \matricula\Service\MatriculaService::getSemestresAll();
        $this->params['semestres'] = $semestres;
        $this->params['title'] = 'Reporte de matricula por semestre';
        $this->params['url_ajax'] = $this->routes()->generate('reportes_ajax');
        $this->params['reportes_cursos'] = $this->routes()->generate('reportes_cursos');
        $this->params['reportes_alumnos'] = $this->routes()->generate('reportes_alumnos');
        $this->params['reportes_docentes'] = $this->routes()->generate('reportes_docentes');
        $this->params['reportes_matriculas'] = $this->routes()->generate('reportes_matriculas');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Reporte')->renderResponse('matricula.html.twig', $this->params);
    }

   

    
    public function downloadSolicitudesAction(Request $request) {
        $this->utilService = new UtilService();
        $desde = $request->attributes->get('desde');
        $hasta = $request->attributes->get('hasta');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator('DACA')
                ->setLastModifiedBy('DACA')
                ->setTitle('Reporte')
                ->setSubject('convalidación')
                ->setDescription('Convalidación')
                ->setKeywords('convalidación ficha solicitud')
                ->setCategory('convalidación');
        //Estilos por defecto
        $spreadsheet->getDefaultStyle()
                ->getFont()
                ->setName('Arial')
                ->setSize(10);
        $styleHeadTitleTable = array(
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'name' => 'Arial',
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'color' => ['rgb' => '000000']
            ],
        );
        //configurar página de impresión
        //$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        //$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        // Add some data
        $spreadsheet->setActiveSheetIndex(0);
        //Armado de los título de la tabla 
        $array_title = [
            'ID',
            'ESTADO DE SOLICITUD',
            'TIPO DE DOCUMENTO',
            'NÚMERO DOCUMENTO',
            'ALUMNO',
            'CÓDIGO ESTUDIANTE',
            'CÓDIGO PERSONA',
            'CÓDIGO POSTULANTE',
            'SEDE DEL POSTULANTE',
            'INSTITUCIÓN PROCEDENCIA',
            'CARRERA PROCEDENCIA',
            'MODALIDAD DE LA CARRERA UPC',
            'CARRERA UPC',
            'CÓDIGO ASIGNATURA PROCEDENCIA',
            'ASIGNATURA PROCEDENCIA',
            'PERIODO',
            'CODIGO ASIGNATURA UPC',
            'NOMBRE ASIGNATURA UPC',
            'MALLA ASIGNATURA UPC',
            'CODIGO CURSO UPC EQUIVALENTE',
            'NOMBRE CURSO UPC EQUIVALENTE',
            'MALLA CURSO UPC EQUIVALENTES',
            'MALLA CURSO POR DIRECTOR',
            'ESTADO DEL CURSO',
            'CONDICIÓN',
            'SUSTENTO',
            'CICLO INCORPORACIÓN',
            'FECHA DE REGISTRO SOLICITUD',
            'USUARIO CREADOR SOLICITUD',
            'SEDE DEL USUARIO CREADOR',
            'TIEMPO TRANSCURRIDO',
            'FECHA VALIDACIÓN',
            'USUARIO VALIDADOR SOLICITUD',
            'TIEMPO TRANSCURRIDO',
            'FECHA DE DERIVACION',
            'FECHA LÍMITE',
            'FECHA DE CONVALIDACIÓN',
            'USUARIO QUE CONVALIDÓ',
            'OBSERVACIÓN DE LA SOLICITUD'
        ];
        $row = 1;
        $col = 1;
        foreach ($array_title as $index => $title) {
            $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
            $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $title, DataType::TYPE_STRING);
            $spreadsheet->getActiveSheet()->getStyle($start_col . $row)->applyFromArray($styleHeadTitleTable);
            $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
            $col++;
        }
        $row++;
        $col = 1;
        //Iteramos las solicitudes
        //obtnemos los 
       
        $solicitudes = $this->getSolicitudesByFecha($desde, $hasta);
        if (is_array($solicitudes) && count($solicitudes) > 0) {
            foreach ($solicitudes as $index => $objSolicitud) {
                foreach ($objSolicitud->detalle as $indice => $objSolicitudDetalle) {
                    
                    $objtiempodederivacion = \convalidacion\Model\SolicitudModel::getTimeDerivacionByIdDetallesoli($objSolicitudDetalle->id);
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_code, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_status, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_tipo_documento, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_documento, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_firstname . ' ' . $objSolicitud->chr_lastname, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_code_student, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_code_persona, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_code_postulante, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    //SEDE
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_sede_postulante, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_carrera_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->code_modalidad, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_carrera_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->cod_curso, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->cursos, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_periodo, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_code_curso_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_curso_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_malla_curso_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;


                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_code_curso_local_equivalente, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_curso_local_equivalente, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_malla_curso_local_equivalente, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->chr_malla_director, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, strip_tags($objSolicitudDetalle->convalida == 'No aplica' ? '' : $objSolicitudDetalle->convalida), DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->condicion == 'No aplica' ? '' : $objSolicitudDetalle->condicion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->sustento, DataType::TYPE_STRING);
                    //$spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->txt_validated, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_cicloincorporacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->fecha_registro, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_name_creator, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_sede_creator, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $this->utilService->timeago($objSolicitud->date_timecreated), DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->fecha_validate, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_name_validador, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $this->utilService->timeago($objSolicitud->date_timevalidate), DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row,$objtiempodederivacion , DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $this->utilService->addDays($objSolicitud->date_timevalidate, 10), DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->date_convalidacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->usuario, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->txt_feedback, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;



                    $row++;
                    $col = 1;
                }
            }
        }
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta));
        $spreadsheet->setActiveSheetIndex(0);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta) . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
    public function downloadNuevoSolicitudesAction(Request $request) {
        $this->utilService = new UtilService();
        $desde = $request->attributes->get('desde');
        $hasta = $request->attributes->get('hasta');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator('DACA')
                ->setLastModifiedBy('DACA')
                ->setTitle('Reporte')
                ->setSubject('convalidación')
                ->setDescription('Convalidación')
                ->setKeywords('convalidación ficha solicitud')
                ->setCategory('convalidación');
        //Estilos por defecto
        $spreadsheet->getDefaultStyle()
                ->getFont()
                ->setName('Arial')
                ->setSize(10);
        $styleHeadTitleTable = array(
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'name' => 'Arial',
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'color' => ['rgb' => '000000']
            ],
        );
        //configurar página de impresión
        //$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        //$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        // Add some data
        $spreadsheet->setActiveSheetIndex(0);
        //Armado de los título de la tabla 
        $array_title = [
            'ID',
            'ESTADO DE SOLICITUD',
            'TIPO DE DOCUMENTO',
            'NÚMERO DOCUMENTO',
            'ALUMNO',
            'CÓDIGO ESTUDIANTE',
            'CÓDIGO PERSONA',
            'CÓDIGO POSTULANTE',
            'SEDE DEL POSTULANTE',
            'INSTITUCIÓN PROCEDENCIA',
            'CARRERA PROCEDENCIA',
            'MODALIDAD DE LA CARRERA UPC',
            'CARRERA UPC',
            'CÓDIGO ASIGNATURA PROCEDENCIA',
            'ASIGNATURA PROCEDENCIA',
            'PERIODO',
            'CODIGO ASIGNATURA UPC',
            'NOMBRE ASIGNATURA UPC',
            'MALLA ASIGNATURA UPC',
            'CODIGO CURSO UPC EQUIVALENTE',
            'NOMBRE CURSO UPC EQUIVALENTE',
            'MALLA CURSO UPC EQUIVALENTES',
            'MALLA CURSO POR DIRECTOR',
            'ESTADO DEL CURSO',
            'CONDICIÓN',
            'SUSTENTO',
            'CICLO INCORPORACIÓN',
            'FECHA DE REGISTRO SOLICITUD',
            'USUARIO CREADOR SOLICITUD',
            'SEDE DEL USUARIO CREADOR',
            'TIEMPO TRANSCURRIDO',
            'FECHA VALIDACIÓN',
            'USUARIO VALIDADOR SOLICITUD',
            'TIEMPO TRANSCURRIDO',
            'FECHA DE DERIVACION',
            'FECHA LÍMITE',
            'FECHA DE CONVALIDACIÓN',
            'USUARIO QUE CONVALIDÓ',
            'OBSERVACIÓN DE LA SOLICITUD'
        ];
        $row = 1;
        $col = 1;
        foreach ($array_title as $index => $title) {
            $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
            $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $title, DataType::TYPE_STRING);
            $spreadsheet->getActiveSheet()->getStyle($start_col . $row)->applyFromArray($styleHeadTitleTable);
            $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
            $col++;
        }
        $row++;
        $col = 1;
        //Iteramos las solicitudes
        //obtnemos los 
       
        $solicitudes = $this->getNuevoSolicitudesByFecha($desde, $hasta);
        if (is_array($solicitudes) && count($solicitudes) > 0) {
            foreach ($solicitudes as $index => $objSolicitud) {
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_status, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_tipo_documento, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_documento, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_alumno, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code_student, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code_person, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code_postulante, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    //SEDE
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_sede_postulante, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_institucion_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_carrera_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_modalidad_carrera_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_carrera_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_codigo_curso_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_curso_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_periodo, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_codigo_curso_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_curso_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_malla_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;


                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_codigo_curso_equivalente, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_curso_equivalente, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_malla_equivalente, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_malla_director, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_curso_status, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_condicion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_sustento, DataType::TYPE_STRING);
                    //$spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitudDetalle->txt_validated, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_ciclo_incorporacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_fecha_registro, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_usuario_creador, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_sede_creador, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_tiempo_transcurrido, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solitud_fecha_validacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_usuario_validador, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_tiempo_transcurrido2, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row,$objSolicitud->chr_solicitud_fecha_derivacion , DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_fecha_limite, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_fecha_convalidacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_usuario_convalido, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_observacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $row++;
                    $col = 1;
            }
        }
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta));
        $spreadsheet->setActiveSheetIndex(0);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta) . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
    
    public function downloadSolicitudesEstadosAction(Request $request) {
        $this->utilService = new UtilService();
        $desde = $request->attributes->get('desde');
        $hasta = $request->attributes->get('hasta');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator('DACA')
                ->setLastModifiedBy('DACA')
                ->setTitle('Reporte')
                ->setSubject('convalidación')
                ->setDescription('Convalidación')
                ->setKeywords('convalidación ficha solicitud')
                ->setCategory('convalidación');
        //Estilos por defecto
        $spreadsheet->getDefaultStyle()
                ->getFont()
                ->setName('Arial')
                ->setSize(10);
        $styleHeadTitleTable = array(
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'name' => 'Arial',
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'color' => ['rgb' => '000000']
            ],
        );
        //configurar página de impresión
        //$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        //$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        // Add some data
        $spreadsheet->setActiveSheetIndex(0);
        //Armado de los título de la tabla 
        $array_title = [
            'ID',
            'ESTADO DE SOLICITUD',
            'DOCUMENTO DE IDENTIDAD',
            'INGRESANTE',
            'CÓDIGO ESTUDIANTE',
            'CÓDIGO PERSONA',
            'CÓDIGO POSTULANTE',
            'SEDE DEL POSTULANTE',
            'INSTITUCIÓN PROCEDENCIA',
            'CARRERA PROCEDENCIA',
            'MODALIDAD DE LA CARRERA UPC',
            'CARRERA UPC',
            'CÓDIGO ASIGNATURA PROCEDENCIA',
            'ASIGNATURA PROCEDENCIA',
            'PERIODO',
            'CODIGO ASIGNATURA UPC',
            'NOMBRE ASIGNATURA UPC',
            'MALLA ASIGNATURA UPC',
            'MALLA CURSO POR DIRECTOR',
            'ESTADO DEL CURSO',
            'ASIGNADO A',
            'CONDICIÓN',
            'SUSTENTO',
            'CICLO INCORPORACIÓN',
            'FECHA DE REGISTRO SOLICITUD',
            'USUARIO CREADOR SOLICITUD',
            'SEDE DEL USUARIO CREADOR',
            //'TIEMPO TRANSCURRIDO',
            'FECHA VALIDACIÓN',
            'USUARIO VALIDADOR SOLICITUD',
            //'TIEMPO TRANSCURRIDO',
            'FECHA DE DERIVACION',
            'FECHA LÍMITE',
            'FECHA DE CONVALIDACIÓN',
            'USUARIO QUE CONVALIDÓ',
            'OBSERVACIÓN DE LA SOLICITUD',
            'RESPONSABLE DE LA REGLA',
            'FECHA SOLICITUD FINALIZADA',
        ];
        $row = 1;
        $col = 1;
        foreach ($array_title as $index => $title) {
            $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
            $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $title, DataType::TYPE_STRING);
            $spreadsheet->getActiveSheet()->getStyle($start_col . $row)->applyFromArray($styleHeadTitleTable);
            $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
            $col++;
        }
        $row++;
        $col = 1;
        //Iteramos las solicitudes
        //obtnemos los 
       
        $solicitudes = $this->getNuevoSolicitudesByFecha($desde, $hasta);
        if (is_array($solicitudes) && count($solicitudes) > 0) {
            foreach ($solicitudes as $index => $objSolicitud) {
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_status, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_documento, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_alumno, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code_student, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code_person, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_code_postulante, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    //SEDE
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_sede_postulante, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_institucion_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_carrera_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_modalidad_carrera_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_carrera_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_codigo_curso_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_curso_procedencia, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_periodo, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_codigo_curso_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_curso_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_malla_local, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_malla_director, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_report_solicitud_status, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_report_asignado_a, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_condicion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_sustento, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_ciclo_incorporacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_fecha_registro, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_usuario_creador, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_sede_creador, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

//                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
//                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_tiempo_transcurrido, DataType::TYPE_STRING);
//                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
//                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solitud_fecha_validacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_usuario_validador, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

//                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
//                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_tiempo_transcurrido2, DataType::TYPE_STRING);
//                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
//                    $col++;
                    
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row,$objSolicitud->chr_solicitud_fecha_derivacion , DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_fecha_limite, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_fecha_convalidacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_usuario_convalido, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_observacion, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_report_responsable_curso, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_solicitud_fecha_finaliza, DataType::TYPE_STRING);
                    $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                    $col++;

                    $row++;
                    $col = 1;
            }
        }
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta));
        $spreadsheet->setActiveSheetIndex(0);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta) . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function downloadFacturacionAction(Request $request) {
        $this->utilService = new UtilService();
        $desde = $request->attributes->get('desde');
        $hasta = $request->attributes->get('hasta');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator('DACA')
                ->setLastModifiedBy('DACA')
                ->setTitle('Reporte')
                ->setSubject('convalidación')
                ->setDescription('Convalidación')
                ->setKeywords('convalidación ficha solicitud')
                ->setCategory('convalidación');
        //Estilos por defecto
        $spreadsheet->getDefaultStyle()
                ->getFont()
                ->setName('Arial')
                ->setSize(10);
        $styleHeadTitleTable = array(
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'name' => 'Arial',
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'color' => ['rgb' => '000000']
            ],
        );
        //configurar página de impresión
        //$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        //$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        // Add some data
        $spreadsheet->setActiveSheetIndex(0);
        //Armado de los título de la tabla 
        $array_title = [
            'COD',
            'CICLO',
            'COD_POSTULANTE',
            'COD_ALUMNO',
            'COD_PERSONAL',
            'APELLIDOS',
            'NOMBRES',
            'CANTIDAD_CURSO_CONVALIDAR'
        ];
        $row = 1;
        $col = 1;
        foreach ($array_title as $index => $title) {
            $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
            $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $title, DataType::TYPE_STRING);
            $spreadsheet->getActiveSheet()->getStyle($start_col . $row)->applyFromArray($styleHeadTitleTable);
            $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
            $col++;
        }
        $row++;
        $col = 1;
        //Iteramos las solicitudes
        //obtenemos los datos
        $solicitudes = $this->getSolicitudesFacturacion($desde, $hasta, \convalidacion\Model\ReportesModel::STATUS_SOLICITUD_FACTURABLE);
        if (is_array($solicitudes) && count($solicitudes) > 0) {
            foreach ($solicitudes as $index => $objSolicitud) {
                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->code_modalidad, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_ciclo, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_code_postulante, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_code_student, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_code_persona, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_lastname, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->chr_firstname, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->num_cursos_convalida, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $row++;
                $col = 1;
            }
        }
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle($this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta));
//        $spreadsheet->createSheet();
//        // Add some data
//        $spreadsheet->setActiveSheetIndex(1)->setCellValue('A1', 'world!');
//        // Rename worksheet
//        $spreadsheet->getActiveSheet()->setTitle('URL Removed');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta) . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function downloadAlumnosAction(Request $request) {
        $this->utilService = new UtilService();
        $desde = $request->attributes->get('desde');
        $hasta = $request->attributes->get('hasta');
        $modalidadid = $request->attributes->get('modalidadid');
        $cicloincorporacionid = $request->attributes->get('cicloincorporacionid');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator('DACA')
                ->setLastModifiedBy('DACA')
                ->setTitle('Reporte')
                ->setSubject('convalidación')
                ->setDescription('Convalidación')
                ->setKeywords('convalidación ficha solicitud')
                ->setCategory('convalidación');
        $styleHeadTitleTable = array(
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'name' => 'Arial',
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'color' => ['rgb' => '000000']
            ],
        );
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        //Obtenemos todos los cursos aprobados de las solicitudes de este intervalo
        $arrayCursos = $this->getCursosOfSolicitudesByFecha($desde, $hasta, \convalidacion\Model\ReportesModel::STATUS_SOLICITUD_FACTURABLE, $modalidadid, $cicloincorporacionid);
        $array_title = ['COD_ALUMNO'];
        $cont = 0;
        //print_object($arrayCursos);die();
        foreach ($arrayCursos as $index => $objCurso) {
            $spreadsheet->createSheet();
            $row = 1;
            $col = 1;
            foreach ($array_title as $index => $title) {
                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->setActiveSheetIndex($cont)->setCellValueExplicitByColumnAndRow($col, $row, $title, DataType::TYPE_STRING);
                $spreadsheet->setActiveSheetIndex($cont)->getStyle($start_col . $row)->applyFromArray($styleHeadTitleTable);
                $spreadsheet->setActiveSheetIndex($cont)->getColumnDimension($start_col)->setAutoSize(true);
                $col++;
            }
            $row++;
            $col = 1;
            //Registramos los COD_ALUMNOS
            if (is_array($objCurso->alumnos) && count($objCurso->alumnos) > 0) {
                foreach ($objCurso->alumnos as $index => $cod_estudiante) {
                    $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                    $spreadsheet->setActiveSheetIndex($cont)->setCellValueExplicitByColumnAndRow($col, $row, $cod_estudiante, DataType::TYPE_STRING);
                    $row++;
                }
            }
            $spreadsheet->getActiveSheet()->setTitle($objCurso->chr_code);
            $cont++;
        }
        $spreadsheet->setActiveSheetIndex(0);
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta) . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
    
    public function downloadConvenioNuevosCursosAlumnosAction(Request $request){
        $this->utilService = new UtilService();
        $desde = $request->attributes->get('desde');
        $hasta = $request->attributes->get('hasta');
        $institucionid = $request->attributes->get('institucionid');
        $roleid = $request->attributes->get('roleid');
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator('DACA')
                ->setLastModifiedBy('DACA')
                ->setTitle('Reporte')
                ->setSubject('convenio')
                ->setDescription('nuevos cursos')
                ->setKeywords('cursos')
                ->setCategory('convenio');
        $styleHeadTitleTable = array(
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'name' => 'Arial',
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'color' => ['rgb' => '000000']
            ],
        );        
        $spreadsheet->setActiveSheetIndex(0);
        //Armado de los título de la tabla 
        $array_title = [
            'CURSO ID',
            'INSTITUCIÓN',
            'CARRERA',
            'DIRECCIÓN',
            'CURSO CÓDIGO',
            'CURSO NOMBRE',
            'CRÉDITOS',
            'PERIODO',
            'URL SÍLABO',
            'FECHA CREACIÓN'
        ];
        $row = 1;
        $col = 1;
        foreach ($array_title as $index => $title) {
            $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
            $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $title, DataType::TYPE_STRING);
            $spreadsheet->getActiveSheet()->getStyle($start_col . $row)->applyFromArray($styleHeadTitleTable);
            $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
            $col++;
        }
        $row++;
        $col = 1;
        //Iteramos las solicitudes
        //obtenemos los datos
        $solicitudes = $this->getCursosNuevos($desde, $hasta, $institucionid, $roleid);
        if (is_array($solicitudes) && count($solicitudes) > 0) {
            foreach ($solicitudes as $index => $objSolicitud) {
                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->curso, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->institucionname, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->carreraname, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->direccion_name, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->cursocode, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->cursoname, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->int_credito_silabo, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $url = $this->utilService->getPathSilabo() . $objSolicitud->name_silabo . '.pdf';
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->periodo, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getCell($start_col.$row)->getHyperlink()->setUrl($url);
                $spreadsheet->getActiveSheet()->getStyle($start_col . $row) ->applyFromArray(array( 'font' => array( 'color' => ['rgb' => '0000FF'], 'underline' => 'single' ) ));                
                //$spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->periodo, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $url, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;
                
                $start_col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
                $spreadsheet->getActiveSheet()->setCellValueExplicitByColumnAndRow($col, $row, $objSolicitud->curso_fecha_creacion, DataType::TYPE_STRING);
                $spreadsheet->getActiveSheet()->getColumnDimension($start_col)->setAutoSize(true);
                $col++;

                $row++;
                $col = 1;
            }
        }
        //***************************************************************************************************************************************
        //***************************************************************************************************************************************
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->utilService->getTimeToDate($desde) . '--' . $this->utilService->getTimeToDate($hasta) . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;        
    }

    public function ajaxAction(Request $request) {
        global $USER;
        $subject = $request->request->get('subject');
        $this->utilService = new UtilService();
        $error = 0;
        $message = '';
        switch ($subject) {
            case 'SelectCurso':
                $inputs = $request->request;
                $ciclo = $inputs->get('inputTxtCiclo');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $cursos = \matricula\Service\CursoService::getCursosAllCiclos($ciclo);
               
                
                $html = $this->template('Reporte')->render('partials/table_cursos.html.twig',[
                    'items' => $cursos
                ]);

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'html' => $html
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'SelectAlumno':
                $inputs = $request->request;
                $cursoid = $inputs->get('inputCurso');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $cursos = \matricula\Service\AlumnoService::getAlumnosAllCursoid($cursoid);
                $html = $this->template('Reporte')->render('partials/table_alumnos.html.twig',[
                    'items' => $cursos
                ]);

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'html' => $html
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'SelectMatricula':
                $inputs = $request->request;
                $semestreid= $inputs->get('inputSemestre');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                 $semestres = \matricula\Service\MatriculaService::getMatriculasAllSemestre($semestreid);
                $html = $this->template('Reporte')->render('partials/table_matricula.html.twig',[
                    'items' => $semestres
                ]);

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'html' => $html
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteCurso':
                $cursoid = $request->request->get('cursoid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarCurso($cursoid);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            
        }
    }

    private function vd($var) {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

}
