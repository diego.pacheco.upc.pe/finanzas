<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Controller;

use matricula\Service\MatriculaService;
use matricula\Service\AlumnoService;
use matricula\Service\UtilService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use core\output\notification;
use context_system;
use moodle_url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class MatriculaController extends MatriculaService {

    private $params;
    private $utilService;

    public function __construct() {
        $this->params = [];
    }

    /**
     * Vista para listar los registros de la tabla sílabos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        //obtenemos las solicitudes
        $matriculas = $this->getMatriculasAll();
        $this->params['matriculas'] = $matriculas;
        $this->params['url_ajax'] = $this->routes()->generate('matriculas_ajax');
        $this->params['url_nueva_matricula'] = $this->routes()->generate('matriculas_editar');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Matricula')->renderResponse('index.html.twig', $this->params);
    }

    public function editarAction(Request $request) {
        $this->params['title'] = "REGISTRAR NUEVA LINEA DE CREDITO";
        //obtenemos el id
        $id = $request->attributes->get('matriculaid');
        //obtenemos el alumno
        if ($id < 1) {
            $objMatricula = $this->getNuevaMatricula();
        } else {
            $objMatricula = $this->getMatriculaById($id);
        }

        if ($objMatricula->int_clienteid > 0) {
            $alumnos = [];
            $alumno = \matricula\Service\AlumnoService::getAlumnoById($objMatricula->int_clienteid);
            array_push($alumnos, $alumno);
        } else {
            $alumnos = \matricula\Service\AlumnoService::getAlumnosAllNoOcupados();
        }
        $ciclos = array("Nominal");
        $ciclos2 = array("MENSUAL", "ANUAL");
        //parametros
        $this->params['objMatricula'] = $objMatricula;
        $this->params['alumnos'] = $alumnos;
        $this->params['ciclos'] = $ciclos;
        $this->params['ciclos2'] = $ciclos2;
        $this->params['url_ajax'] = $this->routes()->generate('matriculas_ajax');
        $this->params['url_home'] = $this->routes()->generate('index');
        $this->params['url_backto'] = $this->routes()->generate('matriculas_index');
        return $this->template('Matricula')->renderResponse('editar.html.twig', $this->params);
    }

    public function ajaxAction(Request $request) {
        $serviceAlumno = new AlumnoService();
        global $USER;
        $subject = $request->request->get('subject');
        $this->utilService = new UtilService();
        $error = 0;
        $message = '';
        switch ($subject) {
            case 'SiguienteFecha':
                $inputs = $request->request;
                $precio = $request->request->get('precio');
                if ($precio > 0) {
                    $this->siguientedia($precio);
                } else {
                    $error = true;
                    $message = 'Ingrese la cantida de dias';
                }
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'GuardarMatricula':
                $inputs = $request->request;
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $id = $inputs->get('inputAlumno');
                $name = $inputs->get('inputTxtDNI');
                $tipo = $inputs->get('TipoTasaInteres');
                $precio = $inputs->get('TasaInteres');
                $precio2 = $inputs->get('PeriodoInteres');
                $precio3 = $inputs->get('Capitalizacion');
                if ($name != " " && $name != "" && $precio != " " && $precio != "" && $id != " " && $id != "") {
                    if ($name > 500 && $name < 1000) {
                        $solicitudid = $this->GuardarMatricula($inputs);
                    } else {
                        $error = true;
                        $message = 'El monto de la linea crediticia debe ser mayor a 500 y menor a 1000';
                    }
                } else {
                    $error = true;
                    $message = 'Complete todos los campos para continuar';
                }

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $solicitudid,
                    'url' => $this->routes()->generate('matriculas_index')
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'AgregarProducto':
                $inputs = $request->request;
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $id = $request->request->get('productoid');
                $compraid = $request->request->get('compraid');
                $cantidad = $request->request->get('cantidad');

                if ($id != " " && $id != "" && $cantidad != " " && $cantidad != "") {
                    $solicitudid = $this->GuardarCompraProducto($id, $compraid, $cantidad);
                } else {
                    $error = true;
                    $message = 'Debe seleccionar un producto';
                }

                $items = $this->getAllCompraProductoByCompraid($compraid);
                $total = $this->getAllTotalCompraProductoByCompraid($compraid);
                $html = $this->template('Matricula')->render('partials/table_compra_productos.html.twig', [
                    'items' => $items,
                    'total' => $total
                ]);

                $response['data'] = [
                    'html' => $html,
                ];
                $response['total'] = $total;
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'AgregarCompra':
                $inputs = $request->request;

                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $id = $request->request->get('productoid');
                $compraid = $request->request->get('compraid');
                $cantidad = $request->request->get('cantidad');
                $efectivo = $request->request->get('efectivo');
                $delivery = $request->request->get('delivery');

                $compra = $this->getCompraById($compraid);
                $items2 = $this->getAllCompraProductoByCompraid($compraid);
                $total2 = $this->getAllTotalCompraProductoByCompraid($compraid);
                $matri = $this->getMatriculaById($compra->int_credito_clienteid);

                if (!empty($items2)) {
                    if ($matri->int_disponible >= ($total2 + $delivery - $efectivo)) {
                        $solicitudid = $this->GuardarCompra($compraid, $efectivo, $delivery);
                    } else {
                        $error = true;
                        $message = 'El total supera su linea de credito actual';
                    }
                } else {
                    $error = true;
                    $message = 'Debe añadir un producto';
                }

                $matriculas = $this->getMatriculasAll();

                $html = $this->template('Matricula')->render('partials/table_matriculas.html.twig', [
                    'matriculas' => $matriculas
                ]);
                $response['data'] = [
                    'html' => $html,
                ];

                $response['id'] = $compra->int_credito_clienteid;
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'AgregarPagoAhora':
                $inputs = $request->request;
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $id = $request->request->get('creditocliente');
                $credito = $this->getMatriculaById($id);
                $pago = $request->request->get('inputPago');
                $compra = $request->request->get('compra');
                if($compra=='Mantenimiento') {
                    if($pago<=$credito->int_mantenimiento){
                        $solicitudid = $this->GuardarPagoMantenimiento($id, $pago);
                    }else {
                            $error = true;
                            $message = 'El pago no puede ser mayor al mantenimiento';
                        }
                    
                    
                } else {

                    if ($pago != "" && $pago != " " && $compra > 0) {
                        $objcompra = $this->getCompraById($compra);
                        if ($pago <= $objcompra->int_total) {
                            $solicitudid = $this->GuardarPago($id, $pago, $objcompra);
                        } else {
                            $error = true;
                            $message = 'El pago no puede ser mayor a la compra a pagar';
                        }
                    } else {
                        $error = true;
                        $message = 'Debe colocar un monton y colocar una compra';
                    }
                }
                $matriculas = $this->getMatriculasAll();

                $html = $this->template('Matricula')->render('partials/table_matriculas.html.twig', [
                    'matriculas' => $matriculas
                ]);
                $response['data'] = [
                    'html' => $html,
                ];

                $response['id'] = $id;
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'GuardarCursosMatricula':
                $data = $request->request->get('data');
                $inputs = $request->request;
                $id = $inputs->get('matriculaid');
                if ($data != null) {
                    $solicitudid = $this->GuardarMatricula($inputs);
                    $docenteid = $this->GuardarMatriculaCurso($id, $data);
                }
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                if ($docenteid == 0) {
                    
                } else {
                    $error = true;
                    $message = 'No puede tener dos cursos a la misma hora';
                }

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $id,
                    'url' => $this->routes()->generate('matriculas_editar', ['matriculaid' => $id])
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteMatricula':
                $matriculaid = $request->request->get('matriculaid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarMatricula($matriculaid);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteCompra':
                $matriculaid = $request->request->get('matriculaid');
                $compra = $this->getCompraById($matriculaid);
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarCompra($matriculaid);

                $matriculas = $this->getMatriculasAll();
                $html = $this->template('Matricula')->render('partials/table_matriculas.html.twig', [
                    'matriculas' => $matriculas
                ]);
                $response['data'] = [
                    'html' => $html,
                ];
                $response['id'] = $compra->int_credito_clienteid;
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deletePago':
                $matriculaid = $request->request->get('matriculaid');
                $compra = $this->getPagoById($matriculaid);
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarPago($matriculaid);

                $matriculas = $this->getMatriculasAll();
                $html = $this->template('Matricula')->render('partials/table_matriculas.html.twig', [
                    'matriculas' => $matriculas
                ]);
                $response['data'] = [
                    'html' => $html,
                ];
                $response['id'] = $compra->int_credito_clienteid;
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteCompraProducto':
                $matriculaid = $request->request->get('matriculaid');

                $compra_producto = $this->getCompraProductoById($matriculaid);
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarCompraMatricula($matriculaid);
                $items = $this->getAllCompraProductoByCompraid($compra_producto->int_compraid);
                $total = $this->getAllTotalCompraProductoByCompraid($compra_producto->int_compraid);
                $html = $this->template('Matricula')->render('partials/table_compra_productos.html.twig', [
                    'items' => $items,
                    'total' => $total
                ]);

                $response['data'] = [
                    'html' => $html,
                ];

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'openModal_estadoCuenta':
                $objid = $request->request->get('id');
//                $compras = $this->getAllPagosByCreditoCliente($objid);
                $credito_cliente = $this->getMatriculaById($objid);
                $cliente = $serviceAlumno->getAlumnoById($credito_cliente->int_clienteid);
                $name = $cliente->chr_first_name . ' ' . $cliente->chr_last_name;
                $items = $this->getAllMovimientos($objid);

                $html = $this->template('Matricula')->render('partials/modal_estado_cuenta.html.twig', [
                    'cliente' => $name,
                    'periodo' => $credito_cliente->chr_periodo,
                    'credito_otorgada' => $credito_cliente->int_credito_monto,
                    'credito_disponible' => $credito_cliente->int_disponible,
                    'deuda' => $credito_cliente->int_debe_monto,
                    'items' => $items,
                    'interes' => $credito_cliente->int_interes
                ]);

                $response['data'] = [
                    'html' => $html
                ];
                $response['message'] = $message;
                $response['error'] = $error;
                $response['success'] = true;
                $status = $response['success'] ? 200 : 500;
                return new JsonResponse($response, $status);
                break;
            case 'showFeedbackMessagePagos':
                $objid = $request->request->get('id');
                $credito = $this->getMatriculaById($objid);
                $compras = $this->getAllPagosByCreditoCliente($objid);
                $alumnos = $this->getAllComprasByCreditoCliente($objid);
                $objMembresia->id = 'Mantenimiento';
                $objMembresia->int_total = $credito->int_mantenimiento;
                array_push($alumnos, $objMembresia);
                $html = $this->template('Matricula')->render('partials/modal_pagos.html.twig', [
                    'compras' => $compras,
                    'alumnos' => $alumnos,
                    'cliente_credito' => $objid
                ]);

                $response['data'] = [
                    'html' => $html
                ];
                $response['message'] = $message;
                $response['error'] = $error;
                $response['success'] = true;
                $status = $response['success'] ? 200 : 500;
                return new JsonResponse($response, $status);
                break;

            case 'showFeedbackMessage':
                $objid = $request->request->get('id');
                $productos = $serviceAlumno->getAlumnosAll();
                $compraid = $this->CrearCompra($objid);
                $compras = $this->getAllComprasByCreditoCliente($objid);

                $html = $this->template('Matricula')->render('partials/modal_compra.html.twig', [
                    'alumnos' => $productos,
                    'total' => 0,
                    'compras' => $compras,
                    'compraid' => $compraid
                ]);

                $response['data'] = [
                    'html' => $html
                ];
                $response['message'] = $message;
                $response['error'] = $error;
                $response['success'] = true;
                $status = $response['success'] ? 200 : 500;
                return new JsonResponse($response, $status);
                break;

            case 'VerProductosDetallado':
                $objid = $request->request->get('id');
                $compra = $this->getCompraById($objid);

                $compras = $this->getAllCompraProductoByCompraid($objid);

                $html = $this->template('Matricula')->render('partials/table_compra_productos2.html.twig', [
                    'items' => $compras,
                    'efectivo' => '-' . $compra->int_efectivo,
                    'delivery' => '+' . $compra->int_delivery,
                    'interes' => '+' . $compra->int_interes_generado
                ]);

                $response['data'] = [
                    'html' => $html
                ];
                $response['message'] = $message;
                $response['error'] = $error;
                $response['success'] = true;
                $status = $response['success'] ? 200 : 500;
                return new JsonResponse($response, $status);
                break;
            case 'deleteMatriculaCurso':
                $cursoid = $request->request->get('data');

                $inputs = $request->request;
                $id = $inputs->get('matriculaid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarMatriculaCurso($cursoid, $id);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $id,
                    'url' => $this->routes()->generate('matriculas_editar', ['matriculaid' => $id])
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
        }
    }

    public function build_source_field($source) {
        $sourcefield = new \stdClass();
        $sourcefield->source = $source;
        return serialize($sourcefield);
    }

}
