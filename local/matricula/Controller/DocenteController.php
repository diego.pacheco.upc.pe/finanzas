<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Controller;

use matricula\Service\DocenteService;
use matricula\Service\UtilService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use core\output\notification;
use context_system;
use moodle_url;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class DocenteController extends DocenteService {

    private $params;
    private $utilService;

    public function __construct() {
        $this->params = [];
    }

    /**
     * Vista para listar los registros de la tabla sílabos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        //obtenemos las solicitudes
        $docentes = $this->getDocentesAll();
        //$modalidades = $this->getModalidadesMenu();
        $this->params['docentes'] = $docentes;
        $this->params['url_ajax'] = $this->routes()->generate('docentes_ajax');
        $this->params['url_editar'] = $this->routes()->generate('docentes_editar');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Docente')->renderResponse('index.html.twig', $this->params);
    }

    public function editarAction(Request $request) {
        $this->params['title'] = "REGISTRAR NUEVO CLIENTE";
        //obtenemos el id
        $id = $request->attributes->get('docenteid');
        //obtenemos el alumno
        if ($id < 1) {
            $objDocente = $this->getNuevoDocente();
        } else {
            $objDocente = $this->getDocenteById($id);
        }
        $disponibles = array("Full time", "Part Time");
        $cursos = $this->getCursosAllDesocupados();
        $cursos_directo = $this->getCursosAllDesocupadosDirector($objDocente->id);
        foreach ($cursos_directo as $selecionados) {
            foreach ($cursos as $diponibles) {
                if ($selecionados->chr_dia == $diponibles->chr_dia && $selecionados->chr_horainicio == $diponibles->chr_horainicio && $selecionados->chr_horafin == $diponibles->chr_horafin) {
                    $diponibles->bloqueado = 1;
                }
            }
        }
        //parametros
        $this->params['cursos_director'] = $cursos_directo;
        $this->params['cursos'] = $cursos;
        $this->params['objDocente'] = $objDocente;
        $this->params['disponibles'] = $disponibles;
        $this->params['url_ajax'] = $this->routes()->generate('docentes_ajax');
        $this->params['url_backto'] = $this->routes()->generate('docentes_index');
        $this->params['url_home'] = $this->routes()->generate('index');
        return $this->template('Docente')->renderResponse('editar.html.twig', $this->params);
    }

    public function ajaxAction(Request $request) {
        global $USER;
        $subject = $request->request->get('subject');
        $this->utilService = new UtilService();
        $error = 0;
        $message = '';
        switch ($subject) {
            case 'GuardarDocente':
                $fallo1 = 0;
                $fallo2 = 0;
                $fallo3 = 0;
                $fallo4 = 0;
                $fallo5 = 0;
                $fallo6 = 0;
                $inputs = $request->request;
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $id = $inputs->get('docenteid');
                if ($id != null) {
                    $objAlumnoid = $this->getDocenteById($id);
                }
                $dni = $inputs->get('inputTxtDNI');
                $chr_correo = $inputs->get('inputTxtcorreo');
                //celular
                $int_celular = $inputs->get('inputTxtcelular');
                $objAlumno = $this->getDocenteByDNI($dni);
                $objAlumnocorreo = $this->getDocenteByCorreo($chr_correo);
                $objAlumnocelular = $this->getDocenteByCelular($int_celular);
                $objAlumnoDocenteDni = \matricula\Service\AlumnoService::getAlumnoByDNI($dni);
                $objAlumnoDocenteCorreo = \matricula\Service\AlumnoService::getAlumnoByCorreo($chr_correo);
                $objAlumnoDocenteCelular = \matricula\Service\AlumnoService::getAlumnoByCelular($int_celular);
                if ($objAlumnoid->id == $objAlumno->id && $objAlumnoid->id == $objAlumnocorreo->id && $objAlumnoid->id == $objAlumnocelular->id && $objAlumnoid->int_dni == $objAlumno->int_dni && $objAlumnoid->chr_correo == $objAlumnocorreo->chr_correo && $objAlumnoid->int_celular == $objAlumnocelular->int_celular && $objAlumnoid->int_dni != $objAlumnoDocenteDni->int_dni && $objAlumnoid->chr_correo != $objAlumnoDocenteCorreo->chr_correo && $objAlumnoid->int_celular != $objAlumnoDocenteCelular->int_celular) {
                    $solicitudid = $this->GuardarDocente($inputs);
                    $response['success'] = true;
                } else {
                    if ($objAlumnoid->id == $objAlumno->id) {
                        if ($inputs->get('inputTxtDNI') != $objAlumnoDocenteDni->int_dni) {
                            $fallo1 = 0;
                        } else {
                            $message .= 'El DNI ya existe. ';
                            $fallo1 = 1;
                            $error = true;
                        }
                    } else {
                        if ($inputs->get('inputTxtDNI') != $objAlumno->int_dni && $inputs->get('inputTxtDNI') != $objAlumnoDocenteDni->int_dni) {
                            $fallo1 = 0;
                        } else {
                            $message .= 'El DNI ya existe. ';
                            $fallo1 = 1;
                            $error = true;
                        }
                    }


                    if ($objAlumnoid->id == $objAlumnocorreo->id) {
                        if ($inputs->get('inputTxtcorreo') != $objAlumnoDocenteCorreo->chr_correo) {
                            $fallo2 = 0;
                        } else {
                            $message .= 'El correo  ya existe. ';
                            $fallo2 = 1;
                            $error = true;
                        }
                    } else {
                        if ($inputs->get('inputTxtcorreo') != $objAlumnocorreo->chr_correo && $inputs->get('inputTxtcorreo') != $objAlumnoDocenteCorreo->chr_correo) {
                            $fallo2 = 0;
                        } else {
                            $message .= 'El correo  ya existe. ';
                            $fallo2 = 1;
                            $error = true;
                        }
                    }
                    if ($objAlumnoid->id == $objAlumnocelular->id) {
                        if ($inputs->get('inputTxtcelular') != $objAlumnoDocenteCelular->int_celular) {
                            $fallo3 = 0;
                        } else {
                            $message .= 'El celular  ya existe. ';
                            $fallo3 = 1;
                            $error = true;
                        }
                    } else {
                        if ($inputs->get('inputTxtcelular') != $objAlumnocelular->int_celular && $inputs->get('inputTxtcelular') != $objAlumnoDocenteCelular->int_celular) {
                            $fallo3 = 0;
                        } else {
                            $message .= 'El celular  ya existe. ';
                            $fallo3 = 1;
                            $error = true;
                        }
                    }
                    if ($fallo1 == 0 && $fallo2 == 0 && $fallo3 == 0) {
                        $solicitudid = $this->GuardarDocente($inputs);
                        $response['success'] = false;
                    }
                }
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $solicitudid,
                    'url' => $this->routes()->generate('docentes_index')
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'GuardarCursos':
                $data = $request->request->get('data');
                $inputs = $request->request;
                $id = $inputs->get('docenteid');
                if($inputs->get('inputTxtName')!=""  && $inputs->get('inputTxtName')!= " " 
                        && $inputs->get('inputTxtApellido')!=""  && $inputs->get('inputTxtApellido')!= " "
                        && $inputs->get('inputTxtDNI')!=""  && $inputs->get('inputTxtDNI')!= " " 
                        && $inputs->get('inputTxtFechaDeNacimiento')!=""  && $inputs->get('inputTxtFechaDeNacimiento')!= " "
                        && $inputs->get('inputTxtcorreo')!=""  && $inputs->get('inputTxtcorreo')!= " "
                        && $inputs->get('inputTxtcelular')!=""  && $inputs->get('inputTxtcelular')!= " "
                        && $inputs->get('inputTxtDisponibilidad')!=""  && $inputs->get('inputTxtDisponibilidad')!= " "
                        && $inputs->get('inputTxtFechaDeIngreso')!=""  && $inputs->get('inputTxtFechaDeIngreso')!= " ") {
                    $fallo1 = 0;
                    $fallo2 = 0;
                    $fallo3 = 0;
                    $fallo4 = 0;
                    $fallo5 = 0;
                    $fallo6 = 0;
                    //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                    if ($id != null) {
                        $objAlumnoid = $this->getDocenteById($id);
                    }
                    $dni = $inputs->get('inputTxtDNI');
                    $chr_correo = $inputs->get('inputTxtcorreo');
                    //celular
                    $int_celular = $inputs->get('inputTxtcelular');
                    $objAlumno = $this->getDocenteByDNI($dni);
                    $objAlumnocorreo = $this->getDocenteByCorreo($chr_correo);
                    $objAlumnocelular = $this->getDocenteByCelular($int_celular);
                    $objAlumnoDocenteDni = \matricula\Service\AlumnoService::getAlumnoByDNI($dni);
                    $objAlumnoDocenteCorreo = \matricula\Service\AlumnoService::getAlumnoByCorreo($chr_correo);
                    $objAlumnoDocenteCelular = \matricula\Service\AlumnoService::getAlumnoByCelular($int_celular);
                    if ($objAlumnoid->id == $objAlumno->id && $objAlumnoid->id == $objAlumnocorreo->id && $objAlumnoid->id == $objAlumnocelular->id && $objAlumnoid->int_dni == $objAlumno->int_dni && $objAlumnoid->chr_correo == $objAlumnocorreo->chr_correo && $objAlumnoid->int_celular == $objAlumnocelular->int_celular && $objAlumnoid->int_dni != $objAlumnoDocenteDni->int_dni && $objAlumnoid->chr_correo != $objAlumnoDocenteCorreo->chr_correo && $objAlumnoid->int_celular != $objAlumnoDocenteCelular->int_celular) {
                        if ($data != null) {

                            $docenteid = $this->GuardarDocenteCurso($id, $data);
                        }
                        //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                        if ($docenteid == 0) {
                            $solicitudid = $this->GuardarDocente($inputs);
                        } else {
                            $error = true;
                            $message = 'No puede tener dos cursos a la misma hora';
                        }
                        $response['success'] = true;
                    } else {
                        if ($objAlumnoid->id == $objAlumno->id) {
                            if ($inputs->get('inputTxtDNI') != $objAlumnoDocenteDni->int_dni) {
                                $fallo1 = 0;
                            } else {
                                $message .= 'El DNI ya existe. ';
                                $fallo1 = 1;
                                $error = true;
                            }
                        } else {
                            if ($inputs->get('inputTxtDNI') != $objAlumno->int_dni && $inputs->get('inputTxtDNI') != $objAlumnoDocenteDni->int_dni) {
                                $fallo1 = 0;
                            } else {
                                $message .= 'El DNI ya existe. ';
                                $fallo1 = 1;
                                $error = true;
                            }
                        }


                        if ($objAlumnoid->id == $objAlumnocorreo->id) {
                            if ($inputs->get('inputTxtcorreo') != $objAlumnoDocenteCorreo->chr_correo) {
                                $fallo2 = 0;
                            } else {
                                $message .= 'El correo  ya existe. ';
                                $fallo2 = 1;
                                $error = true;
                            }
                        } else {
                            if ($inputs->get('inputTxtcorreo') != $objAlumnocorreo->chr_correo && $inputs->get('inputTxtcorreo') != $objAlumnoDocenteCorreo->chr_correo) {
                                $fallo2 = 0;
                            } else {
                                $message .= 'El correo  ya existe. ';
                                $fallo2 = 1;
                                $error = true;
                            }
                        }
                        if ($objAlumnoid->id == $objAlumnocelular->id) {
                            if ($inputs->get('inputTxtcelular') != $objAlumnoDocenteCelular->int_celular) {
                                $fallo3 = 0;
                            } else {
                                $message .= 'El celular  ya existe. ';
                                $fallo3 = 1;
                                $error = true;
                            }
                        } else {
                            if ($inputs->get('inputTxtcelular') != $objAlumnocelular->int_celular && $inputs->get('inputTxtcelular') != $objAlumnoDocenteCelular->int_celular) {
                                $fallo3 = 0;
                            } else {
                                $message .= 'El celular  ya existe. ';
                                $fallo3 = 1;
                                $error = true;
                            }
                        }
                        if ($fallo1 == 0 && $fallo2 == 0 && $fallo3 == 0) {
                            if ($data != null) {

                                $docenteid = $this->GuardarDocenteCurso($id, $data);
                            }
                            //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                            if ($docenteid == 0) {
                                $solicitudid = $this->GuardarDocente($inputs);
                            } else {
                                $error = true;
                                $message = 'No puede tener dos cursos a la misma hora';
                            }
                            $response['success'] = false;
                        }
                    }
                } else {
                    $error = true;
                    $message = 'No puede haber campos vacios';
                }

                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $response['data'] = [
                    'id' => $id,
                    'url' => $this->routes()->generate('docentes_editar', ['docenteid' => $id])
                ];
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteDocente':
                $docenteid = $request->request->get('docenteid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarDocente($docenteid);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
            case 'deleteDocenteCurso':
                $cursoid = $request->request->get('cursoid');
                //Agregamos validaciones en el servidor para evitar registrar datos inválidos
                $this->eliminarDocenteCurso($cursoid);
                $response['success'] = true;
                $response['error'] = $error;
                $response['message'] = $message;
                $status = $response['success'] ? 200 : 500; // Status code de la respuesta.
                return new JsonResponse($response, $status);
                break;
        }
    }

    public function build_source_field($source) {
        $sourcefield = new \stdClass();
        $sourcefield->source = $source;
        return serialize($sourcefield);
    }

}
