<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Model;

/**
 * Class HomeModel.
 * Convalidaciones
 * =======
 * Los convalidaciones se encarga de añadir una fecha de inicio y fin a cada grupo dentro de un curso.
 *
 * @copyright  2018 UPC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class DocenteModel {

 
    /**
     * Método para cargar el objeto de solicitud por su ID
     * @global object $DB
     * @param int $id
     * @return object
     */
    public static function getDocentesAll() {
        global $DB;
        $sql = "select *from mdl_mtc_cliente where is_active=1 and is_deleted=0";
        return $DB->get_records_sql($sql);
    }
    
    
    public static function getCursosAllDesocupados() {
        global $DB;
        $sql = "select c.*, "
                . " ca.chr_name as carrera "
                . "from mdl_mtc_curso c "
                . " inner join mdl_mtc_carrera ca on ca.id = c.int_carreraid "
                . " where c.is_active=1 and c.is_deleted=0 and c.is_ocupado2=0";
        return $DB->get_records_sql($sql);
    }
    
    
    public static function getCursosAllDesocupadosDirector($id){
        global $DB;
        $sql = "select c.*, "
                . " ca.chr_name as carrera "
                . " from mdl_mtc_curso_docente d "
                . " inner join mdl_mtc_curso c on c.id=d.int_cursoid "
                . " inner join mdl_mtc_carrera ca on ca.id = c.int_carreraid "
                . " where d.is_active=1 and d.is_deleted=0 and d.int_docenteid=$id";
        return $DB->get_records_sql($sql);
    }
    
    public static function getDocenteById($id) {
        global $DB;
        $sql = "select * from mdl_mtc_cliente where is_active=1 and is_deleted=0 and id=$id";
        return $DB->get_record_sql($sql);
    }
    
     public static function getDocenteByCorreo($dni) {
        global $DB;
        $sql = "select * from mdl_mtc_cliente where is_active=1 and is_deleted=0 and chr_correo='".$dni."'";
        return $DB->get_record_sql($sql);
    }
    
    public static function getDocenteByCelular($dni) {
        global $DB;
        $sql = "select * from mdl_mtc_cliente where is_active=1 and is_deleted=0 and int_celular='".$dni."'";
        return $DB->get_record_sql($sql);
    }
    
    public static function getDocenteByDNI($dni) {
        global $DB;
        $sql = "select * from mdl_mtc_cliente where is_active=1 and is_deleted=0 and int_dni=$dni";
        return $DB->get_record_sql($sql);
    }
    
    public static function saveDocente($obj) {
        global $DB;
        $returnValue = $DB->insert_record('mtc_cliente', $obj);
        return $returnValue;
    }
    
    public static function saveDocenteCurso($obj) {
        global $DB;
        $returnValue = $DB->insert_record('mtc_curso_docente', $obj);
        return $returnValue;
    }
    
    
    public static function updateDocente($obj) {
        global $DB;
        $returnValue = $DB->update_record('mtc_cliente', $obj);
        return $returnValue;
    }
    
    
    
     public static function updateDocenteCurso($id) {
        global $DB;
        $sql = "UPDATE mdl_mtc_curso_docente SET is_active=0 , is_deleted= 1 where id=(select id from mdl_mtc_curso_docente where is_active=1 and is_deleted=0 and int_cursoid= $id)";
        $DB->execute($sql);
        return $id;
    }
    
    public static function updateDocenteCursoByDecenteid($id) {
        global $DB;
        $sql = "UPDATE mdl_mtc_curso_docente SET is_active=0 , is_deleted= 1 where id in (select concat(id) from mdl_mtc_curso_docente where is_active=1 and is_deleted=0 and int_docenteid= $id)";
        $DB->execute($sql);
        return $id;
    }
    
    
     public static function updateCursoByDecenteid($id) {
        global $DB;
        $sql="UPDATE mdl_mtc_curso set is_ocupado2 = 0 where id in (select concat(int_cursoid) from mdl_mtc_curso_docente where int_docenteid= $id)";
        $DB->execute($sql);
        return $id;
    }
    
    
      public static function updateCursoByCursoid($id) {
        global $DB;
        $sql="UPDATE mdl_mtc_curso set is_ocupado2 = 0 where id = $id";
        $DB->execute($sql);
        return $id;
    }
    

    

}
