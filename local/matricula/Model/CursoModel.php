<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Model;

/**
 * Class HomeModel.
 * Convalidaciones
 * =======
 * Los convalidaciones se encarga de añadir una fecha de inicio y fin a cada grupo dentro de un curso.
 *
 * @copyright  2018 UPC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class CursoModel {

 
    /**
     * Método para cargar el objeto de solicitud por su ID
     * @global object $DB
     * @param int $id
     * @return object
     */
     public static function getCursosAll() {
        global $DB;
        $sql = "select c.*, "
                . " concat(c.chr_first_name,' ',c.chr_last_name) as carrera "
                . " from mdl_mtc_cliente c where c.is_active=1 and c.is_deleted=0";
        return $DB->get_records_sql($sql);
    }
    
    
     public static function getCursosAllCiclos($ciclo) {
        global $DB;
        $sql = "select c.* from mdl_mtc_credito_cliente cc "
                . " inner join mdl_mtc_compra c on c.int_credito_clienteid = cc.id "
                . " where c.is_active=1 and c.is_deleted=0 and  cc.is_active=1 and cc.is_deleted=0 and cc.int_clienteid= $ciclo";
       
        return $DB->get_records_sql($sql);
    }
    
    public static function getCursoEstasiendoUsado($id) {
        global $DB;
        $sql = "select * from mdl_mtc_curso where is_active=1 and is_deleted=0 and id=$id";
        return $DB->get_record_sql($sql);
    }
    
     public static function getCursoById($id) {
        global $DB;
        $sql = "select * from mdl_mtc_curso where is_active=1 and is_deleted=0 and id=$id";
        return $DB->get_record_sql($sql);
    }
    
    public static function getCursosById($id) {
        global $DB;
        $sql = "select * from mdl_mtc_curso where is_active=1 and is_deleted=0 and id in ($id)";
        return $DB->get_records_sql($sql);
    }
    
    
    public static function getCursoBySeccion($id) {
        global $DB;
        $sql = "select * from mdl_mtc_curso where is_active=1 and is_deleted=0 and chr_seccion='".$id."'";
        return $DB->get_record_sql($sql);
    }
    
     public static function getCursoByCodigo($id) {
        global $DB;
        $sql = "select * from mdl_mtc_curso where is_active=1 and is_deleted=0 and chr_code='".$id."'";
        return $DB->get_record_sql($sql);
    }
    
    public static function saveCurso($obj) {
        global $DB;
        $returnValue = $DB->insert_record('mtc_curso', $obj);
        return $returnValue;
    }
    
    public static function updateCurso($obj) {
        global $DB;
        $returnValue = $DB->update_record('mtc_curso', $obj);
        return $returnValue;
    }

    
    public static function updateCompra($obj) {
        global $DB;
        $returnValue = $DB->update_record('mtc_compra', $obj);
        return $returnValue;
    }
    
    
    

}
