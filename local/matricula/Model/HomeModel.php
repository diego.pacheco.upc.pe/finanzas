<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Model;

/**
 * Class HomeModel.
 * matriculaes
 * =======
 * Los matriculaes se encarga de añadir una fecha de inicio y fin a cada grupo dentro de un curso.
 *
 * @copyright  2017 UPC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class HomeModel {
    
    const PACKAGE_MATRICULA = 1;
    
    public static function getAllSeccions(){
        global $DB;
        $sql = "select s.*
                from mdl_ma_seg_seccion s
                inner join mdl_ma_seg_modulo m on m.id = s.int_moduloid and m.int_packageid = '".self::PACKAGE_MATRICULA."'
                where s.is_active=1 and s.is_deleted=0
                order by s.int_indexorder";
        
        return $DB->get_records_sql($sql);
    }
    
    
}
