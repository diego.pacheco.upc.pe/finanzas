<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Model;

/**
 * Class HomeModel.
 * Convalidaciones
 * =======
 * Los convalidaciones se encarga de añadir una fecha de inicio y fin a cada grupo dentro de un curso.
 *
 * @copyright  2017 UPC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ReporteModel {

    const STATUS_CONVALIDACION_APROBADA = 1;
    const STATUS_CONVALIDACION_RECHAZADA = 2;
    const STATUS_SOLICITUD_BORRADOR = 3;
    const INSTITUCION_LOCAL = 1;
    const ROLE_ADMISION = 11;
    const ROLE_ADMISION_FRONT = 21;
    const ROLE_ADMISION_JUNIOR = 22;
    const STATUS_SOLICITUD_PENDIENTE_POR_FACTURAR = 25;
    const STATUS_SOLICITUD_FACTURABLE = 26;
    const STATUS_AGRAPACION_NOAPLICA = 27;
    const STATUS_AGRAPACION_APROBADA = 28;
    const STATUS_AGRAPACION_RECHAZADA = 29;
    const ID_DATO_CAMPUS_USER = 5;

    /**
     * listamos las solicitudes en ese intervalo de tiempo
     * @global object $DB
     * @param int $desde timestamp
     * @param int $hasta timestamp
     * @param int $status
     * @return array
     */
    public static function getSolicitudesByFechas($desde, $hasta, $status = 0, $modalidadid = 0, $cicloincorporacionid = 0) {
        global $DB;
        //date_format(from_unixtime(s_detalle.date_timedirector_direccion), \"%d/%m/%Y\") as tiempo_derivacion,
        //left join mdl_con_cvl_solicitud_detalle s_detalle on s_detalle.int_solicitudid = s.id and s_detalle.is_active= 1 and s_detalle.is_deleted =0
        $sql = "select 
            s.*, 
            date_format(from_unixtime(s.date_timecreated), \"%d/%m/%Y\") as fecha_registro, 
            if(s.date_timevalidate > 0, date_format(from_unixtime(s.date_timevalidate), \"%d/%m/%Y\"),'No aplica')  as fecha_validate, 
            m.chr_code as code_modalidad,
            st.chr_name as chr_status,
            ip.chr_name as chr_procedencia, 
            ipl.chr_name as chr_local, 
            camp.chr_name as chr_sede_postulante, 
            cp.chr_name as chr_carrera_procedencia,
            lcp.id as id_carrera_local,
            lcp.chr_name as chr_carrera_local,
            concat(u.firstname,' ', u.lastname) as chr_name_creator,
            userinfo.data as chr_sede_creator,
            concat(u2.firstname,' ', u2.lastname) as chr_name_validador,
            td.chr_name as chr_tipo_documento,
            p.chr_name as chr_cicloincorporacion
            from mdl_con_cvl_solicitud s 
            inner join mdl_con_mst_modalidad m on m.id = s.int_modalidadid 
            inner join mdl_con_cvl_status st on st.id = s.int_statusid
            inner join mdl_con_pro_institucion ip on ip.id = s.procedenciaid
            inner join mdl_con_pro_carrera cp on cp.id = s.int_carreraid_from
            inner join mdl_user u on u.id = s.int_usuario_creador
            left join mdl_user_info_data userinfo on userinfo.userid = u.id and userinfo.fieldid =" . self::ID_DATO_CAMPUS_USER . "
            inner join mdl_con_mst_tipo_documento td on td.id = s.int_tipo_documentoid
            inner join mdl_con_pro_institucion ipl on ipl.id = " . self::INSTITUCION_LOCAL . "
            inner join mdl_con_pro_carrera lcp on lcp.id = s.int_carreraid_to
            left join mdl_con_mst_campus camp on camp.id = s.int_campusid 
            left join mdl_user u2 on u2.id = s.int_usuario_validador
            left join mdl_con_mst_periodo p on p.id = s.int_cicloincorporacionid
            where s.is_active=1 ";
        if ($status > 0) {
            $sql .= " and s.int_statusid= " . $status;
        }
        if ($modalidadid > 0) {
            $sql .= " and s.int_modalidadid= " . $modalidadid;
        }
        if ($cicloincorporacionid > 0) {
            $sql .= " and s.int_cicloincorporacionid= " . $cicloincorporacionid;
        }
        $sql .= " and s.is_deleted=0 and s.date_timecreated 
            between '" . $desde . "' and '" . $hasta . "' 
            order by s.date_timecreated asc";
        //var_dump($sql);die();
        return $DB->get_records_sql($sql);
    }

    public static function getCursosNuevos($desde, $hasta, $institucionid, $roleid) {
        global $DB;
        $returnValue = [];
        $sql = "select mc.id, c.id as curso, i.chr_name as institucionname,d.chr_code as cod_direccion, d.chr_name as direccion_name,c.chr_code as cursocode, c.chr_name as cursoname,
                date_format(from_unixtime(c.date_timecreated), \"%d/%m/%Y %r\") as curso_fecha_creacion
                , ca.chr_name as carreraname, ca.chr_code as code_carrera, mc.int_creditos as int_credito_malla,
                s.int_creditos as int_credito_silabo, p.chr_name as periodo, p.id as int_periodoid,s.chr_name as name_silabo
                from mdl_con_pro_curso c
                inner join mdl_con_pro_direccion d on d.id = c.int_direccionid and d.is_active=1 and d.is_deleted=0
                inner join mdl_con_pro_institucion i on i.id = d.int_institucionid and i.is_active=1 and i.is_deleted=0
                inner join mdl_con_pro_malla_curso mc on mc.int_cursoid = c.id and mc.is_active=1 and mc.is_deleted=0
                inner join mdl_con_pro_malla m on m.id = mc.int_mallaid and m.is_active=1 and m.is_deleted=0
                inner join mdl_con_pro_carrera ca on ca.id = m.int_carreraid and ca.is_active =1 and ca.is_deleted=0
                left join mdl_con_pro_silabo s on s.int_mallacursoid = mc.id and s.is_active=1 and s.is_deleted=0
                left join mdl_con_mst_periodo p on p.id = s.int_periodoid
                where c.is_active=1 and c.is_deleted=0
                and c.int_creatorid in (select userid from mdl_role_assignments where roleid=" . $roleid . ")
                and i.id=" . $institucionid . " and c.date_timecreated between '" . $desde . "' and '" . $hasta . "' order by ca.chr_name,c.chr_name,p.chr_shortname asc";
        $rs = $DB->get_recordset_sql($sql);
        foreach ($rs as $record) {
            array_push($returnValue, $record);
        }
        $rs->close();
        return $returnValue;
    }

    public static function getIdDetalleSolicitud($array_id_solicitud = []) {
        global $DB;
        $sql = "select * from mdl_con_cvl_solicitud_detalle where is_active=1 and is_deleted=0 and int_solicitudid in (" . implode(',', $array_id_solicitud) . ")";
        //var_dump($sql);die();
        return $DB->get_records_sql($sql);
    }

    public static function getIdAgrupacionesConva($array_id_solicitd_detalle = []) {
        global $DB;
        if (is_array($array_id_solicitd_detalle) && count($array_id_solicitd_detalle) == 0) {
            array_push($array_id_solicitd_detalle, 0);
        }
        $sql = "select * from mdl_con_cvl_sol_conva where is_active=1 and is_deleted=0 and int_solicitud_detalleid in (" . implode(',', $array_id_solicitd_detalle) . ")";
        return $DB->get_records_sql($sql);
    }

    public static function getAgrupacionesAprobadasByIds($array_id_agrupaciones) {
        global $DB;
        if (is_array($array_id_agrupaciones) && count($array_id_agrupaciones) == 0) {
            array_push($array_id_agrupaciones, 0);
        }
        $sql = "select * from mdl_con_cvl_sol_agrupacion where is_active=1 and is_deleted=0 and id in (" . implode(',', $array_id_agrupaciones) . ") and int_statusid ='" . self::STATUS_CONVALIDACION_APROBADA . "'";
        return $DB->get_records_sql($sql);
    }

    public static function getCursosByAgrupación($array_id_agrupaciones) {
        global $DB;
        $sql = "select * from mdl_con_cvl_sol_agru_detalle where is_active=1 and is_deleted=0 and int_agrupacionid in (" . implode(',', $array_id_agrupaciones) . ") and is_procedencia ='0'";
        return $DB->get_records_sql($sql);
    }

    public static function getCursosByIds($array_id_curso) {
        global $DB;
        $sql = "select * from mdl_con_pro_curso where is_active=1 and is_deleted=0 and id in (" . implode(',', $array_id_curso) . ")";
        return $DB->get_records_sql($sql);
    }

    public static function getIdAgrupacionesByCurso($int_cursoid, $array_id_agrupaciones) {
        global $DB;
        $sql = "select * from mdl_con_cvl_sol_agru_detalle where is_active=1 and is_deleted=0 and int_agrupacionid in (" . implode(',', $array_id_agrupaciones) . ") and is_procedencia=0 and int_cursoid=" . $int_cursoid;
        return $DB->get_records_sql($sql);
    }

    public static function getConvasByAgrupaciones($array_id_agrupaciones_curso) {
        global $DB;
        $sql = "select * from mdl_con_cvl_sol_conva where is_active=1 and is_deleted=0 and int_agrupacionid in (" . implode(',', $array_id_agrupaciones_curso) . ")";
        return $DB->get_records_sql($sql);
    }

    public static function getIdSolicitudesByArrayIdSolicituDetalle($array_id_solicitudes_detalle) {
        global $DB;
        $sql = "select * from mdl_con_cvl_solicitud_detalle where id in (" . implode(',', $array_id_solicitudes_detalle) . ") and is_active=1 and is_deleted=0";
        return $DB->get_records_sql($sql);
    }

    public static function getSolicitudesByIds($array_id_solicitud) {
        global $DB;
        $sql = "select * from mdl_con_cvl_solicitud where id in (" . implode(',', $array_id_solicitud) . ") and is_active=1 and is_deleted=0";
        return $DB->get_records_sql($sql);
    }

    public static function getDetalleSolicitudBySolicitudId($solicitudid) {
        global $DB;
        $sql = "select * from mdl_con_cvl_solicitud_detalle where is_active=1 and is_deleted=0 and int_solicitudid =" . $solicitudid;
        return $DB->get_records_sql($sql);
    }

    public static function getConvasBySolicitudDetalle($array_id_detalle) {
        global $DB;
        $sql = "select * from mdl_con_cvl_sol_conva where is_active=1 and is_deleted=0 and int_solicitud_detalleid in (" . implode(',', $array_id_detalle) . ")";
        return $DB->get_records_sql($sql);
    }

    public static function getTopAdmision($limit = 10) {
        global $DB;
        $sql = "select concat(c.chr_first_name,' ',c.chr_last_name) as chr_name , cc.int_debe_monto as num from mdl_mtc_credito_cliente cc
inner join mdl_mtc_cliente c on c.id= cc.int_clienteid
where cc.is_active=1 and cc.is_deleted=0 and c.is_active=1 and c.is_deleted=0 and cc.int_debe_monto>0
order by cc.int_debe_monto desc limit 10";
        return $DB->get_records_sql($sql);
    }

    public static function getTopAlumnos($limit = 10) {
        global $DB;
        $sql = "select concat(c.chr_first_name,' ',c.chr_last_name) as chr_name , cc.int_credito_monto as num from mdl_mtc_credito_cliente cc
inner join mdl_mtc_cliente c on c.id= cc.int_clienteid
where cc.is_active=1 and cc.is_deleted=0 and c.is_active=1 and c.is_deleted=0
order by cc.int_credito_monto desc limit 10";
        return $DB->get_records_sql($sql);
    }

    public static function getTopProcedencias($limit = 5) {
        global $DB;
//        $sql = "select a.chr_name) as chr_name, count(m.id) as num from mdl_mtc_docente a"
//                . " left join mdl_mtc_curso_docente m on m.int_docenteid=a.id  and m.is_active=1 and m.is_deleted=0 "
//                . " order by num desc limit 10 ";
//        return $DB->get_records_sql($sql);
    }

    public static function getSolicitudDetalleBySolicitudId($solicitudid) {
        global $DB;
        global $DB;
//        $sql = "select 
//                sd.*,
//                s.chr_name as chr_status,
//                mp.chr_name as chr_periodo
//                from mdl_con_cvl_solicitud_detalle sd
//                inner join mdl_con_cvl_status s on s.id = sd.int_statusid and s.is_active=1 and s.is_deleted=0
//                inner join mdl_con_mst_periodo mp on mp.id = sd.int_periodoid and mp.is_active = 1 and mp.is_deleted=0
//                where sd.id =" . $solicitudid;
//        return $DB->get_record_sql($sql);        
        return $DB->get_records('con_cvl_solicitud_detalle', ['int_solicitudid' => $solicitudid, 'is_active' => 1, 'is_deleted' => 0]);
    }

    public static function getNuevoSolicitudesByFecha($desde, $hasta, $status = 0) {
        global $DB;
        $sql = "select * from mdl_con_rep_solicitud where date_timecreated between '" . $desde . "' and '" . $hasta . "'";
        return $DB->get_records_sql($sql);
    }

    public static function getAgrupacionByIdWithMalla($id) {
        global $DB;
        $sql = "select a.*, sg.int_mallaid mallaid_director, m.chr_name as chr_malla_director, m.chr_code as chr_code_malla_director  
                from mdl_con_cvl_sol_agrupacion a
                left join mdl_con_cvl_solicitud_grupo sg on sg.id = a.int_grupoid
                left join mdl_con_pro_malla m on m.id = sg.int_mallaid
                where a.id =" . $id;
        return $DB->get_record_sql($sql);
    }

    public static function triggerSolicitudById($solicitudid) {
        global $CFG;
        //Obtenemos el path absoluto del php
        $path = 'php ' . $CFG->dirroot . '/local/convalidacion/cli/reportes.php ' . $solicitudid . ' > /dev/null 2>/dev/null &';
        $returnValue = shell_exec($path);
    }

    public static function getCursoById($cursoid) {
        global $DB;
        return $DB->get_record('con_pro_curso', ['id' => $cursoid]);
    }

}
