<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Model;

/**
 * Class UtilModel.
 * matriculaes
 * =======
 * Los matriculaes se encarga de añadir una fecha de inicio y fin a cada grupo dentro de un curso.
 *
 * @copyright  2018 UPC
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class UtilModel {
    
    const ROLE_SHORTNAME_DIRECTOR = 'directorcarrera';

    const ROLE_SHORTNAME_NUEVO_ROL = 'nuevorol';
    
    const ROLE_SHORTNAME_DIRECTOR_ACADEMICO = 'directordireccionacademica';
    
    const ROLE_SHORTNAME_ADMISION = 'admision';
    const ROLE_SHORTNAME_ADMISION_FRONT = 'admisionregistrosolicitud';
    const ROLE_SHORTNAME_ESTUDIANTE_LIBRE = 'estudiantelibre';
    const ROLE_SHORTNAME_PROSPECCION= 'credorconvenio';
    
    const ROLE_SHORTNAME_VRAI = 'vrai';    
    
    const UPC = 1;
    
    const REQUISITO_SIN_REQUISITO = 1;
    const REQUISITO_CURSO = 2;
    const REQUISITO_CREDITOS_APROBADOS = 3;
    const REQUISITO_NIVELES = 4;
    const REQUISITO_GRUPO = 5;
    const REQUISITO_CUSTOM = 6;
    
    const CAMPUS_TODAS_LAS_SEDES = 'Todas las sedes';
    const CAMPUS_SIN_SEDE = 'Sin sede';
    const CAMPUS_LCT= 'LCT';
    const CAMPUS_MONTERRICO = 'Campus Monterrico';
    const CAMPUS_VILLA = 'Campus Villa';
    const CAMPUS_SAN_ISIDRO = 'Campus San Isidro';
    const CAMPUS_SAN_MIGUEL = 'Campus San Miguel'; 
    const CAMPUS_TELEMARKETING = 'Telemarketing';
    
    const PROFILE_FIELD_CAMPUS = 5;
    const MALLA_CURSO_ELECTIVO = 2;
    const MALLA_CURSO_OBLIGATORIO = 1;

    public static function getRolesByUser($userid){
        global $DB;
        return $DB->get_records('role_assignments',['userid'=>$userid]);
    }
    
    public static function getRoleDirector(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_DIRECTOR],'*', IGNORE_MULTIPLE);
    }

    public static function getRoleNuevoRol(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_NUEVO_ROL],'*', IGNORE_MULTIPLE);
    }

    public static function getRoleDirectorAcademico(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_DIRECTOR_ACADEMICO],'*', IGNORE_MULTIPLE);
    }
    
    public static function getRoleVrai(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_VRAI],'*', IGNORE_MULTIPLE);
    }
    
    public static function getRoleFrontAdmision(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_ADMISION_FRONT],'*', IGNORE_MULTIPLE);
    }
    
    
    public static function getRolesAdmision(){
        global $DB;
        $sql = "select * from {role} where shortname like '%admision%' order by sortorder";
        return $DB->get_records_sql($sql);
    }
    public static function getRoleAdmision(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_ADMISION],'*', IGNORE_MULTIPLE);
    }
    
    public static function getRoleEstudianteLibre(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_ESTUDIANTE_LIBRE],'*', IGNORE_MULTIPLE);
    }
    
    public static function getRoleProspeccion(){
        global $DB;
        return $DB->get_record('role',['shortname'=>self::ROLE_SHORTNAME_PROSPECCION],'*', IGNORE_MULTIPLE);
    }
    
    public static function getUsuarioByRole($roleid){
        global $DB, $USER;
        return $DB->get_record('role_assignments',['roleid'=>$roleid,'userid'=>$USER->id],'*', IGNORE_MULTIPLE);
    }
    
    public static function getProcedenciaByCarreraId($carreraId) {
        global $DB;
        $sql = "SELECT 
                i.*
                FROM mdl_con_pro_institucion i
                INNER JOIN mdl_con_pro_facultad f ON f.int_institucionid = i.id AND f.is_active=1 AND f.is_deleted=0
                INNER JOIN mdl_con_pro_carrera c ON c.int_facultadid= f.id AND c.is_active=1 AND c.is_deleted=0
                WHERE i.is_active=1 AND i.is_deleted=0 AND c.id =" . $carreraId;
        return $DB->get_record_sql($sql);
    }

    public static function getPeriodosByIds($array_periodos){
        global $DB;
        $sql = "SELECT * from {con_mst_periodo} WHERE id IN (". implode(',', $array_periodos).")";
        return $DB->get_records_sql($sql); 
    }
    
    public static function getUserById($id){
        global $DB;
        return $DB->get_record('user',['id'=>$id]);
    }
    
    public static function getMallaCursoById($mallacursoid){
        global $DB;
        return $DB->get_record('con_pro_malla_curso',['id'=>$mallacursoid],'*', IGNORE_MULTIPLE);
    }
    
    public static function getRequisitoByMallacurso($mallacursoid){
        global $DB;
        return $DB->get_record('con_pro_curso_requisito',['int_mallacursoid'=>$mallacursoid,'is_active'=>1,'is_deleted'=>0],'*', IGNORE_MULTIPLE);
    }
    
    public static function getRequisitosByMallacurso($mallacursoid){
        global $DB;
        return $DB->get_records('con_pro_curso_requisito',['int_mallacursoid'=>$mallacursoid,'is_active'=>1,'is_deleted'=>0]);
    }
    
    public static function getCursoById($id){
        global $DB;
        return $DB->get_record('con_pro_curso',['id'=>$id]);
    }
    
    public static function getDetalleRequisito($requisitoid){
        global $DB;
        return $DB->get_records('con_pro_curso_req_detalle',['int_cursorequisitoid'=>$requisitoid,'is_active'=>1,'is_deleted'=>0]);
    }
    
    public static function getSedeById($sedeid=0){
        global $DB;
        return $DB->get_record('con_mst_campus',['id'=>$sedeid]);
    }
    
    public static function getTipoCondiciones(){
        global $DB;
        return $DB->get_records('con_mst_tipo_condicion',['is_active'=>1,'is_deleted'=>0]);
    }
    
    public static function getRoles(){
        global $DB;
        $sql = "select * from mdl_role where id > 8";// Listamos todos los roles nuevos creados fuera de moodle
        return $DB->get_records_sql($sql);
    }
    
    public static function getSilabosByCursoIdByPeriodoId($cursoid, $periodoid){
        global $DB;
        $sql = "select 
                mc.id, mc.int_mallaid, mc.int_cursoid
                 ,ps.int_periodoid as periodo_silabo, c.id as int_carreraid
                 , c.chr_code as code_carrera, c.chr_name as name_carrera,
                  ps.chr_name as name_silabo
                from mdl_con_pro_malla_curso mc
                inner join mdl_con_pro_malla m on m.id = mc.int_mallaid and m.is_active=1 and m.is_deleted=0
                left join mdl_con_pro_carrera c on c.id = m.int_carreraid and c.is_active=1 and c.is_deleted=0
                left join mdl_con_pro_silabo ps on ps.int_cursoid = mc.int_cursoid and ps.int_mallacursoid=mc.id and ps.is_active=1 and ps.is_deleted=0
                where mc.int_cursoid='".$cursoid."' and mc.is_active=1 and mc.is_deleted=0
                and ps.int_periodoid=".$periodoid;
        return $DB->get_records_sql($sql);
    }
    
    public static function getStatusById($statusid){
        global $DB;
        return $DB->get_record('con_cvl_status',['id'=>$statusid]);
    }
    
    public static function getPeriodoById($periodoid){
        global $DB;
        return $DB->get_record('con_mst_periodo',['id'=>$periodoid]);
    }
}