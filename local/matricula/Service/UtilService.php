<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Service;

use matricula\Core\Template;
use matricula\Model\HomeModel;
use matricula\Model\UtilModel;
use matricula\Service\AdministracionService;
use context_system;
use moodle_url;
use stdClass;
use matricula\Service\SolicitudService;

class UtilService extends Template {

    private $path_silabo = "";
    private $uri_silabo = "/local/matricula/Resources/silabos/";
    static private $valid_exts = ['pdf', 'PDF'];
    static private $max_size = 200000 * 1024;

    const ROLE_DIRECTOR_CARRERA = 9;
    const ROLE_DIRECTOR_DIRECCION_ACADEMICA = 10;
    const ROLE_DECANO = 12;
    const ROLE_ASISTENTE_DIRECTOR = 24;
    const ROLE_ADMISION_BACKOFFICE = 11;
    const ROLE_ADMISION_FRONTOFFICE = 21;
    const ROLE_ADMISION_JUNIOR = 22;
    const BASE_PATH_CONVENIO = '/local/matricula/Resources/convenios/';

    public function getPathSilabo() {
        global $CFG;
        $this->path_silabo = $CFG->wwwroot . $this->uri_silabo;
        return $this->path_silabo;
    }

    /**
     * Método para listar un array de formatos o extensiones habilitadas
     * @return array
     */
    public static function getValidExts() {
        return self::$valid_exts;
    }

    public static function getExtensionExcel() {
        return ['xlsx'];
    }

    public static function getBaseConvenio() {
        global $CFG;
        return $CFG->dirroot . self::BASE_PATH_CONVENIO;
    }

    public static function getUrlConvenio() {
        global $CFG;
        return $CFG->wwwroot . self::BASE_PATH_CONVENIO;
    }

    /**
     * Método que retorna el tamaño máximo del archivo a subir
     * @return int
     */
    public static function getMaxSize() {
        return self::$max_size;
    }

    public function vd($var) {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    /**
     * Metodo para obtener la traducción de la variable $strLan
     * @param string $strLan
     * @return string
     */
    public function getString($strLan) {
        return get_string($strLan, 'local_matricula');
    }

    public function getUriConveniosHome() {
        return $this->routes()->generate('convenio_index');
    }

    public function getUriSubmitSilabo() {
        return $this->routes()->generate('curso_submit');
    }

    public function getUriIndex() {
        return $this->routes()->generate('index');
    }

    public function getUriEditConvenio($id) {
        return $this->routes()->generate('convenio_editar', ['convenioid' => $id]);
    }

    public function getUriReporteFicha($id) {
        return $this->routes()->generate('consultas_ficha_reporte', ['id' => $id]);
    }

    public function getUriConvenioCursos($id) {
        return $this->routes()->generate('convenio_cursos', ['convenioid' => $id]);
    }

    public function getUriConvenio() {
        return $this->routes()->generate('convenio_index');
    }

    public static function getBaseDocumento($id) {
        $ObjServiceConvenio = new SolicitudService();
        $url = '';
        //obtenemos el objeto sílabo
        $objConvenioFile = \matricula\Model\ConvenioModel::getConvenioFileById($id);
        $path_silabo_temporal = $ObjServiceConvenio->getBasePathConvenios() . $objConvenioFile->chr_name . '.pdf';

        if (!file_exists($path_silabo_temporal)) {
            $color = 'color:red;';
            $title = 'El sílabo de este curso no existe';
            $url = ' ';
        } else {
            if (is_object($objConvenioFile)) {
                //$record = \matricula\Model\SolicitudModel::getRecordById($objSilaboFile->int_fileid);
                //$url_moodle = moodle_url::make_pluginfile_url($record->contextid, $record->component, $record->filearea, $record->itemid, $record->filepath, $record->filename)->out(false);
                //$url='<a href="'.$url_moodle.'">'.$record->filename.'</a>';
                $url_moodle = self::getUrlConvenio() . $objConvenioFile->chr_name . '.' . $objConvenioFile->chr_extension;
                $url = '<a href="' . $url_moodle . '" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 20px;"></i></a>';
            }
        }
        return $url;
    }

    public static function getDocumento($id) {
        $url = '';
        //obtenemos el objeto sílabo
        $objConvenioFile = \matricula\Model\ConvenioModel::getConvenioFileById($id);
        if (is_object($objConvenioFile)) {
            //$record = \matricula\Model\SolicitudModel::getRecordById($objSilaboFile->int_fileid);
            //$url_moodle = moodle_url::make_pluginfile_url($record->contextid, $record->component, $record->filearea, $record->itemid, $record->filepath, $record->filename)->out(false);
            //$url='<a href="'.$url_moodle.'">'.$record->filename.'</a>';
            $url_moodle = self::getUrlConvenio() . $objConvenioFile->chr_name . '.' . $objConvenioFile->chr_extension;
            $url = '<a href="' . $url_moodle . '" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 20px;"></i></a>';
        }
        return $url;
    }

    public function getRole($userid = 0) {
        global $USER;

        $returnValue = [];
        if ($userid == 0) {
            $userid = $USER->id;
        }
        //Obtenemos los roles del usuario
        $roles = UtilModel::getRolesByUser($userid);
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if (!in_array($objRole->roleid, $returnValue)) {
                    array_push($returnValue, $objRole->roleid);
                }
            }
        }
        return $returnValue;
    }

    public function isDirectorCarrera($userid = 0) {
        global $USER;
        $returnValue = FALSE;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        $roles = UtilModel::getRolesByUser($userid);
        //iteramos los roles y verificamos si es director de carrera
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if ($objRole->roleid == self::ROLE_DIRECTOR_CARRERA) {
                    $returnValue = TRUE;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function isAsistenteDirector($userid = 0) {
        global $USER;
        $returnValue = FALSE;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        $roles = UtilModel::getRolesByUser($userid);
        //iteramos los roles y verificamos si es director de carrera
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if ($objRole->roleid == self::ROLE_ASISTENTE_DIRECTOR) {
                    $returnValue = TRUE;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function isDirectorDireccionAcademica($userid = 0) {
        global $USER;
        $returnValue = FALSE;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        $roles = UtilModel::getRolesByUser($userid);
        //iteramos los roles y verificamos si es director de carrera
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if ($objRole->roleid == self::ROLE_DIRECTOR_DIRECCION_ACADEMICA) {
                    $returnValue = TRUE;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function isDecano($userid = 0) {
        global $USER;
        $returnValue = FALSE;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        $roles = UtilModel::getRolesByUser($userid);
        //iteramos los roles y verificamos si es director de carrera
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if ($objRole->roleid == self::ROLE_DECANO) {
                    $returnValue = TRUE;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function isAdmisionBackOffice($userid = 0) {
        global $USER;
        $returnValue = FALSE;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        $roles = UtilModel::getRolesByUser($userid);
        //iteramos los roles y verificamos si es director de carrera
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if ($objRole->roleid == self::ROLE_ADMISION_BACKOFFICE) {
                    $returnValue = TRUE;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function isAdmisionFrontOffice($userid = 0) {
        global $USER;
        $returnValue = FALSE;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        $roles = UtilModel::getRolesByUser($userid);
        //iteramos los roles y verificamos si es director de carrera
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if ($objRole->roleid == self::ROLE_ADMISION_FRONTOFFICE) {
                    $returnValue = TRUE;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function isAdmisionJunior($userid = 0) {
        global $USER;
        $returnValue = FALSE;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        $roles = UtilModel::getRolesByUser($userid);
        //iteramos los roles y verificamos si es director de carrera
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                if ($objRole->roleid == self::ROLE_ADMISION_JUNIOR) {
                    $returnValue = TRUE;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function getUriCursoSilabo($cursoid, $carreraid) {
        return $this->routes()->generate('curso_silabo', ['cursoid' => $cursoid, 'carreraid' => $carreraid]);
    }

    public function getUriVerCurso($id) {
        return $this->routes()->generate('curso_ver', ['id' => $id]);
    }

    public function getUriByName($name) {
        return $this->routes()->generate($name);
    }

    public function getUriIcon($icon, $ext = 'png') {
        global $CFG;
        return $CFG->wwwroot . '/local/matricula/Resources/icons/' . $icon . '.' . $ext;
    }

    public function getUriImages($images, $ext = 'png') {
        global $CFG;
        return $CFG->wwwroot . '/local/matricula/Resources/images/' . $images . '.' . $ext;
    }

    public function getUriSilabo() {
        global $CFG;
        return $CFG->wwwroot . '/local/matricula/Resources/silabos/';
    }

    public function is_directorCarrera() {
        $returnValue = FALSE;
        //obtenemos el id del rol de director de carrera
        $objRole = UtilModel::getRoleDirector();
        if (is_object($objRole)) {
            //verificamos si el usuario tiene asignado este rol
            $objRegistroUsuarioRol = UtilModel::getUsuarioByRole($objRole->id);
            if (is_object($objRegistroUsuarioRol)) {
                $returnValue = TRUE;
            }
        }
        return $returnValue;
    }

    public function is_admision() {
        $returnValue = FALSE;
        //obtenemos el id del rol de director de carrera
        $objRole = UtilModel::getRoleAdmision();
        if (is_object($objRole)) {
            //verificamos si el usuario tiene asignado este rol
            $objRegistroUsuarioRol = UtilModel::getUsuarioByRole($objRole->id);
            if (is_object($objRegistroUsuarioRol)) {
                $returnValue = TRUE;
            }
        }
        return $returnValue;
    }

    public function is_estudinteLibre() {
        $returnValue = FALSE;
        //obtenemos el id del rol de director de carrera
        $objRole = UtilModel::getRoleEstudianteLibre();
        if (is_object($objRole)) {
            //verificamos si el usuario tiene asignado este rol
            $objRegistroUsuarioRol = UtilModel::getUsuarioByRole($objRole->id);
            if (is_object($objRegistroUsuarioRol)) {
                $returnValue = TRUE;
            }
        }
        return $returnValue;
    }

    public function is_directorAcademico() {
        $returnValue = FALSE;
        //obtenemos el id del rol de director de carrera
        $objRole = UtilModel::getRoleDirectorAcademico();
        if (is_object($objRole)) {
            //verificamos si el usuario tiene asignado este rol
            $objRegistroUsuarioRol = UtilModel::getUsuarioByRole($objRole->id);
            if (is_object($objRegistroUsuarioRol)) {
                $returnValue = TRUE;
            }
        }
        return $returnValue;
    }

    public function getRolesUserCurrent() {
        $returnValue = [];
        $roles = \matricula\Model\ConvenioModel::getRolesUserCurrent();
        if (is_array($roles) && count($roles) > 0) {
            foreach ($roles as $index => $objRole) {
                array_push($returnValue, $objRole->roleid);
            }
        }
        return $returnValue;
    }

    public function has_access() {
        $returnValue = FALSE;
        if (is_siteadmin()) {
            $returnValue = TRUE;
        } else {
            //Obtenemos todos los roles para la plataforma
            $roles = UtilModel::getRoles();
            $array_id_role_user = $this->getRolesUserCurrent();
            if (is_array($roles) && count($roles) > 0) {
                foreach ($roles as $index => $objRole) {
                    if (in_array($objRole->id, $array_id_role_user)) {
                        $returnValue = TRUE;
                    }
                }
            }
            //varificamos si tiene algún rol registrado
            //verificamos si es director de la carrera
//            if ($this->is_directorCarrera()) {
//                $returnValue = TRUE;
//            } else {
//                if ($this->is_directorAcademico()) {
//                    $returnValue = TRUE;
//                } else {
//                    if ($this->is_admision()) {
//                        $returnValue = TRUE;
//                    }else{
//                        if($this->is_estudinteLibre()){
//                            $returnValue = TRUE;
//                        }
//                    }
//                }
//            }
        }
        return $returnValue;
    }

    public function is_admin() {
        return is_siteadmin();
    }

    public function getUriEditMalla($id) {
        return $this->routes()->generate('malla_editar', ['id' => $id]);
    }

    public function getUriVerMalla($id) {
        return $this->routes()->generate('malla_ver', ['id' => $id]);
    }

    public function getUriVerCursosMalla($id) {
        return $this->routes()->generate('cursos_malla_ver', ['id' => $id]);
    }

    public function getUriAprobarConvenio($id) {
        return $this->routes()->generate('convenio_aprobacion', ['convenioid' => $id]);
    }

    public function is_procedencia($carreraid) {
        $returnValue = 1;
        $objProcedencia = UtilModel::getProcedenciaByCarreraId($carreraid);
        if ($objProcedencia->id == UtilModel::UPC) {
            $returnValue = 0;
        }
        return $returnValue;
    }

    public function getClass($carreraid) {
        $returnValue = 'table-success';
        if ($this->is_procedencia($carreraid)) {
            $returnValue = 'table-danger';
        }
        return $returnValue;
    }

    /**
     * Método para buscar y retornar los nombres de los periodos
     * @param string $periodos
     * @return string
     */
    public function getNamePeriodos($periodos) {
        $returnValue = $periodos;
        $array_temp = [];
        if (strlen(trim($periodos)) > 0) {
            $array_periodos = explode(',', $periodos);
            $listaPeriodos = UtilModel::getPeriodosByIds($array_periodos);
            if (is_array($listaPeriodos) && count($listaPeriodos) > 0) {
                foreach ($listaPeriodos as $index => $objPeriodo) {
                    array_push($array_temp, $objPeriodo->chr_name);
                }
                $returnValue = implode(', ', $array_temp);
            }
        }
        return $returnValue;
    }

    public function printArray($array, $is_unique = 0, $n = '<br>') {
        $returnValue = '';
        if (is_array($array) && count($array) > 0) {
            if ($is_unique) {
                $array = array_unique($array);
            }
            $returnValue = implode($n, $array);
        }
        return $returnValue;
    }

    public function getNameUsuario($userid) {
        $returnValue = 'No aplica';
        if ($userid > 0) {
            $objUser = UtilModel::getUserById($userid);
            if (is_object($objUser)) {
                $returnValue = $objUser->firstname . ' ' . $objUser->lastname;
            }
        }
        return $returnValue;
    }

    public function getColor($cicloid) {
        $returnValue = 'table-light';
        //validamos si es par o impar
        if ($cicloid > 0) {
            if ($cicloid % 2 == 0) {
                //par
                $returnValue = 'table-dark';
            } else {
                $returnValue = 'table-light';
            }
        }
        return $returnValue;
    }

    public function getRequisito($mallacursoid) {
        $returnValue = '';
        //primero verificamos si el malla curso existe un requisito de texto
        $objMallaCurso = UtilModel::getMallaCursoById($mallacursoid);
        if (is_object($objMallaCurso) && strlen(trim($objMallaCurso->chr_prerequisito)) > 0) {
            $returnValue = $objMallaCurso->chr_prerequisito;
        } else {
            $objCursoRequisito = UtilModel::getRequisitoByMallacurso($mallacursoid);
            if (is_object($objCursoRequisito)) {//validamos que al menos existe un requisito
                $requisitos = UtilModel::getRequisitosByMallacurso($mallacursoid);
                if (is_array($requisitos) && count($requisitos) > 0) {
                    $cont = 0;
                    foreach ($requisitos as $index => $objRequisito) {
                        if ($cont > 0) {
                            $returnValue .= ' ' . $objRequisito->chr_relacion . ' ';
                        }
                        $returnValue .= $this->getRequisitoValue($objRequisito);
                        $cont++;
                    }
                }
            }
        }
        return $returnValue;
    }

    public function getRequisitoValue($objCursoRequisito) {
        $returnValue = 'No aplica';
        //$objCursoRequisito = UtilModel::getRequisitoByMallacurso($mallacursoid);
        if (is_object($objCursoRequisito)) {
            switch ($objCursoRequisito->int_requisitoid) {
                case UtilModel::REQUISITO_SIN_REQUISITO:
                    $returnValue = 'Sin requisito';
                    break;
                case UtilModel::REQUISITO_CURSO:
                    $objCurso = UtilModel::getCursoById($objCursoRequisito->chr_value);
                    if (is_object($objCurso)) {
                        $returnValue = '<strong>[' . $objCurso->chr_code . ']</strong> ' . $objCurso->chr_name;
                    }
                    break;
                case UtilModel::REQUISITO_CREDITOS_APROBADOS:
                    $returnValue = $objCursoRequisito->chr_value . ' créditos aprobados';
                    break;
                case UtilModel::REQUISITO_NIVELES:
                    $returnValue = $objCursoRequisito->chr_value . ' niveles aprobados';
                    break;
                case UtilModel::REQUISITO_GRUPO:
                    $returnValue = '(';
                    $returnValue .= $this->getRequisitoGrupoValue($objCursoRequisito->id);
                    $returnValue .= ')';
                    break;
                case UtilModel::REQUISITO_CUSTOM:
                    $returnValue = $objCursoRequisito->chr_value;
                    break;
                default:
                    $returnValue = 'No aplica';
                    break;
            }
        }
        return $returnValue;
    }

    public function getRequisitoGrupoValue($requisitoid) {
        $returnValue = '';
        $detalleRequisitos = UtilModel::getDetalleRequisito($requisitoid);
        if (is_array($detalleRequisitos) && count($detalleRequisitos) > 0) {
            $cont = 0;
            foreach ($detalleRequisitos as $index => $objDetalleRequisito) {
                if ($cont > 0) {
                    $returnValue .= ' ' . $objDetalleRequisito->chr_relacion . ' ';
                }
                $returnValue .= $this->getDetalleRequisitoValue($objDetalleRequisito);
                $cont++;
            }
        }
        return $returnValue;
    }

    public function getDetalleRequisitoValue($objDetalleRequisito) {
        $returnValue = '';
        if (is_object($objDetalleRequisito)) {
            switch ($objDetalleRequisito->int_requisitoid) {
                case UtilModel::REQUISITO_SIN_REQUISITO:
                    $returnValue = 'Sin requisito';
                    break;
                case UtilModel::REQUISITO_CURSO:
                    $objCurso = UtilModel::getCursoById($objDetalleRequisito->chr_value);
                    if (is_object($objCurso)) {
                        $returnValue = '<strong>[' . $objCurso->chr_code . ']</strong> ' . $objCurso->chr_name;
                    }
                    break;
                case UtilModel::REQUISITO_CREDITOS_APROBADOS:
                    $returnValue = $objDetalleRequisito->chr_value . ' créditos aprobados';
                    break;
                case UtilModel::REQUISITO_NIVELES:
                    $returnValue = $objDetalleRequisito->chr_value . ' niveles aprobados';
                    break;
                case UtilModel::REQUISITO_CUSTOM:
                    $returnValue = $objDetalleRequisito->chr_value;
                    break;
                default:
                    $returnValue = '';
                    break;
            }
        }
        return $returnValue;
    }

    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function timeago($timestamp) {
        if (is_null($timestamp) || strlen(trim($timestamp)) == 0) {
            return '';
        } else {
            $strTime = array("segundo", "minuto", "hora", "día", "mes", "año");
            $length = array("60", "60", "24", "30", "12", "10");
            $currentTime = time();
            if ($currentTime >= $timestamp) {
                $diff = time() - strip_tags($timestamp);
                for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
                    $diff = $diff / $length[$i];
                }
                $diff = round($diff);
                return "Hace " . $diff . " " . $strTime[$i] . "(s)";
            }
        }
    }

    public function addDays($timestamp, $days = 10) {
        if (is_null($timestamp) || strlen(trim($timestamp)) == 0) {
            return '';
        } else {
            $date = date('m/d/Y', $timestamp);
            return date('d/m/Y', strtotime($date . ' + ' . $days . ' days'));
        }
    }

    public function getFecha($timestamp) {
        $returnValue = '-';
        if ($timestamp > 0) {
            $returnValue = date('d/m/Y H:i:s', $timestamp);
        }
        return $returnValue;
    }

    public function getNameSede($campusid = 0) {
        $returnValue = 'No aplica';
        if ($campusid > 0) {
            $objSede = UtilModel::getSedeById($campusid);
            if (is_object($objSede)) {
                $returnValue = $objSede->chr_title;
            }
        }
        return $returnValue;
    }

    public function getTipoCondiciones() {
        return UtilModel::getTipoCondiciones();
    }

    public function getTimeToDate($time) {
        return date("Y-m-d", $time);
    }

    public function getIndiceCicloByMonth($month) {
        $array['01'] = '00';
        $array['02'] = '00';
        $array['03'] = '00';
        $array['04'] = '00';
        $array['05'] = '01';
        $array['06'] = '01';
        $array['07'] = '01';
        $array['08'] = '01';
        $array['09'] = '02';
        $array['10'] = '02';
        $array['11'] = '02';
        $array['12'] = '02';
        return $array[$month];
    }

    public function getCicloByFecha($time) {
        $year = date("Y", $time);
        $month = date("m", $time);
        $indice = $this->getIndiceCicloByMonth($month);
        return $year . $indice;
    }

    public function getStatusGrupoAprobado() {
        return \matricula\Model\matriculaModel::STATUS_GRUPO_APROBADO;
    }

    public function getStatusGrupoCreado() {
        return \matricula\Model\matriculaModel::STATUS_GRUPO_CREADO;
    }

    public function getStatusConvenio($convenioid) {
        $returnValue = 0;
        $objConvenio = \matricula\Model\ConvenioModel::getConvenioById($convenioid);
        if (is_object($objConvenio)) {
            $returnValue = $objConvenio->int_statusid;
        }
        return $returnValue;
    }

    public function getStatusConvenioBorrador() {
        return \matricula\Model\ConvenioModel::STATUS_CONVENIO_INICIADO;
    }

    public function getNameInstitucionById($procedenciaid) {
        return \matricula\Model\ConvenioModel::getNameProcedenciaById($procedenciaid);
    }

    public function getNameCarreraById($carreraid) {
        return \matricula\Model\ConvenioModel::getNameCarreraById($carreraid);
    }

    public function getUriCondicion($id) {
        return $this->routes()->generate('condicion_ver', ['solicitudid' => $id]);
    }

    /**
     * Desarrolla pila de links de una convalidación y curso
     * @param int $cursoid
     * @param int $periodoid
     * @return string
     */
    public function getSilabosByCursoIdPeriodoId($cursoid, $periodoid, $matriculaid) {
        $returnValue = '';
        $array_link = [];
        $array_temp = [];
        //Obtemos las carreras que tienen silabo con este curso y este periodo
        $silabos_carrera = UtilModel::getSilabosByCursoIdByPeriodoId($cursoid, $periodoid);
        if (is_array($silabos_carrera) && count($silabos_carrera) > 0) {
            foreach ($silabos_carrera as $index => $objSilaboCarrera) {
                $a = '<a href="' . $this->getPathSilabo() . $objSilaboCarrera->name_silabo . '.pdf" target="_blank"><small>' . $objSilaboCarrera->name_carrera . '</small></a>';
                $array_link[$objSilaboCarrera->int_carreraid] = $a;
            }
        }
        $carreras = $this->getCarrerasByConvalidaciónId($matriculaid);
        //print_object($carreras);;
        foreach ($carreras as $puntero => $objCarrera) {
            if (isset($array_link[$objCarrera->int_carreraid])) {
                $array_temp[] = $array_link[$objCarrera->int_carreraid];
            } else {
                $array_temp[] = '<small>' . $objCarrera->chr_name . '</small>';
            }
        }

        if (count($array_temp) > 0) {
            $returnValue = implode(',', $array_temp);
        }
        return $returnValue;
    }

    /**
     * Listamos las carreras a las que aplica esta convalidación
     * @param int $matriculaid
     * @param boolean $is_procedencia
     * @return array
     */
    public function getCarrerasByConvalidaciónId($matriculaid, $is_procedencia = 1) {
        return \matricula\Model\AdministracionModel::getCarrerasByConvalidaciónIdList($matriculaid, $is_procedencia);
    }

    public function getProcedenciaLocalId() {
        return UtilModel::UPC;
    }

    /**
     * @param int $number
     * @return string
     */
    function numberToRomanRepresentation($number) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function getCodeCursoPeriodo($cursoid, $periodoid, $carreraid) {
        $objperiodo = \matricula\Model\matriculaModel::getPeriodoById($periodoid);
        $objcurso = \matricula\Model\matriculaModel::getCursoById($cursoid);
        return $carreraid . '_' . $this->clean($objcurso->chr_code) . '-' . $objperiodo->chr_shortname;
    }

    public function getChrStatusName($statusid) {
        $returnValue = '';
        $objStatus = UtilModel::getStatusById($statusid);
        if (is_object($objStatus)) {
            $returnValue = $objStatus->chr_actions;
        }
        return $returnValue;
    }

    public function getNameDirectorByDireccion($direccionid) {
        $returnValue = '';
        $objAdministracionService = new AdministracionService();
        $returnValue = $objAdministracionService->getDirectoresName($direccionid);
        return $returnValue;
    }

    public function getNameDirectorByCarrera($carreraid) {
        $returnValue = '';
        $objAdministracionService = new AdministracionService();
        $objCarrera = \matricula\Model\AdministracionModel::getCarreraById($carreraid);
        if (is_object($objCarrera)) {
            $returnValue = $objAdministracionService->getDirectorCarreraName($carreraid);
        }
        return $returnValue;
    }

    public function getNameDecanoByCarrera($carreraid) {
        $returnValue = '';
        $objAdministracionService = new AdministracionService();
        $objCarrera = \matricula\Model\AdministracionModel::getCarreraById($carreraid);
        if (is_object($objCarrera)) {
            $returnValue = $objAdministracionService->getDecanosName($objCarrera->int_facultadid);
        }
        return $returnValue;
    }

    public function getNameCicloIncorporacion($periodoid) {
        $returnValue = '';
        $objPeriodo = UtilModel::getPeriodoById($periodoid);
        if (is_object($objPeriodo)) {
            $returnValue = $objPeriodo->chr_name;
        }
        return $returnValue;
    }

    public function isElectivo($objetoItem) {
        $returnValue = false;
        if ($objetoItem->int_tipo_cursoid == UtilModel::MALLA_CURSO_ELECTIVO) {
            $returnValue = true;
        }
        return $returnValue;
    }

    public function getColorElectivos($cicloid) {
        $returnValue = 'bg-white';
        if ($cicloid->int_tipo_cursoid == UtilModel::MALLA_CURSO_ELECTIVO) {
            $returnValue = 'bg-warning';
        }
        return $returnValue;
    }

    public function getStatusCursoRetornado() {
        return \matricula\Model\matriculaModel::STATUS_CURSO_RETORNADO_DIRECTOR_DIRECCION;
    }

    public function getStatusCursoDerivado() {
        return \matricula\Model\matriculaModel::STATUS_CURSO_DERIVADO_DIRECTOR_DIRECCION;
    }
    
    public function getStatusValidarCurso($status, $array_condiciones=[], $array_solicitudes=[], $objSolicitud){
        $returnValue = $status;
        //Verificamos si está en blacklist
        if($objSolicitud->is_blacklist){
            $returnValue = $array_solicitudes[\matricula\Model\matriculaModel::STATUS_GRUPO_NO_APLICA_REGLA]->chr_name;
        }else{
            if($objSolicitud->int_statusid == \matricula\Model\matriculaModel::STATUS_FICHA_matricula_RECHAZADA){
                $returnValue = $array_solicitudes[\matricula\Model\matriculaModel::STATUS_FICHA_matricula_RECHAZADA]->chr_name;
            }else{
                if(is_array($array_condiciones) && count($array_condiciones)>0){
                    $returnValue = $array_solicitudes[\matricula\Model\matriculaModel::STATUS_CONDICION_APROBADA]->chr_name;
                }            
            }            
        }
        return $returnValue;
    }
    
    public function isSuperAdministrador(){
        global $USER;
        $returnValue = FALSE;
        if(is_siteadmin()){//Validamos que al menos sea administrador de la plataforma
            //verificamos que sea del grupo de super adminsitradores
            $super_adminsitradores = ['admin','dipachec'];
            if(in_array($USER->username, $super_adminsitradores)){
                $returnValue = TRUE;
            }
        }
        return $returnValue;
    }

}
