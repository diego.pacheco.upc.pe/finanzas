<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Service;

use matricula\Core\Template;
use matricula\Model\ReporteModel;
use matricula\Service\UtilService;
use context_system;
use moodle_url;
use stdClass;

class ReporteService extends Template {
    
    private $utilService;
    private $convalidacionService;
    
    public function getNuevoSolicitudesByFecha($desde, $hasta, $status=0){
        return ReporteModel::getNuevoSolicitudesByFecha($desde, $hasta, $status=0);
    }


    public function getSolicitudesByFecha($desde, $hasta, $status=0){
        $this->convalidacionService = new ConvalidacionService();
        $solicitudes = ReporteModel::getSolicitudesByFechas($desde, $hasta, $status);
        if(is_array($solicitudes) && count($solicitudes)>0){
            foreach($solicitudes as $index=>$objSolicitud){
                $resultado = $this->convalidacionService->getSeguimientoBySolicitudId($objSolicitud->id,$objSolicitud->id_carrera_local);
                $objSolicitud->detalle = $resultado['data']['objSolicitud']->detalle;
                $solicitudes[$index] = $objSolicitud;                
            }   
            
        }
        return $solicitudes;
    }
    
    public function getSeguimientoBySolicitudId($objSolicitud) {
        global $USER;
        $this->utilService = new UtilService();
        $this->solicitudService = new SolicitudService();
        $this->convalidacionService = new ConvalidacionService();
        $returnValue = [];
        $array_id_cursos_solicitados = [];
        $array_id_periodos_solicitados = [];
        $error = 0;
        $message = '';
        $data = [];
        if (is_object($objSolicitud)) {
            $objSolicitud = $this->getSolicitudToSeguimiento($objSolicitud);
            $detalles = $objSolicitud->detalle;
            //iteramos los cursos para darle información de los estados
            switch ($objSolicitud->int_statusid) {
                case \convalidacion\Model\ConvalidacionModel::STATUS_SOLICITUD_INICIADO://pendiente por validar
                    if (is_array($detalles) && count($detalles) > 0) {
                        foreach ($detalles as $index => $objCursoDetalleSolicitud) {
                            //$objCursoDetalleSolicitud->convalida = $this->convalidacionService->getTextConvalida($objCursoDetalleSolicitud, $objSolicitud->int_statusid); //($objCursoDetalleSolicitud->is_valid == 0 || $objCursoDetalleSolicitud->is_valid == -1) ? 'Rachazado por admisión'.' - <i><small>'.$this->utilService->timeago($objCursoDetalleSolicitud->date_timevalidated).'</small></i>'  : $this->getStatusName($objCursoDetalleSolicitud->int_statusid);
                            $arrayResultEstado = $this->convalidacionService->getTextConvalidaNew($objCursoDetalleSolicitud, $objSolicitud->int_statusid, $objSolicitud);
                            $objCursoDetalleSolicitud->convalida = $arrayResultEstado['html_status'];
                            $objCursoDetalleSolicitud->report_solicitud_status = $arrayResultEstado['chr_status'];
                            $objCursoDetalleSolicitud->report_solicitud_asignado_a = $arrayResultEstado['chr_asignadoa'];
                            $objCursoDetalleSolicitud->report_responsable_regla = $this->getNameResponsableRegla($objCursoDetalleSolicitud->int_cursoid_local);
                            
                            $objCursoDetalleSolicitud->sustento = $this->convalidacionService->getTextSustento($objCursoDetalleSolicitud); //($objCursoDetalleSolicitud->is_valid == 0 || $objCursoDetalleSolicitud->is_valid == -1) ? $objCursoDetalleSolicitud->txt_validated : '-';
                            $arrayResultCondicion = $this->convalidacionService->getCondicion($objCursoDetalleSolicitud);
                            $objCursoDetalleSolicitud->condicion = $arrayResultCondicion['condicion'];
                            
                             $returnUsuario = $this->convalidacionService->getUsuario($objCursoDetalleSolicitud, $objSolicitud->int_statusid);
                             $objCursoDetalleSolicitud->usuario = $returnUsuario['usuario'];
                             $objCursoDetalleSolicitud->date_convalidacion = $returnUsuario['date'];
                            $detalles[$index] = $objCursoDetalleSolicitud;
                        }
                    }
                    break;
                case \convalidacion\Model\ConvalidacionModel::STATUS_SOLICITUD_RECHAZADO:
                    if (is_array($detalles) && count($detalles) > 0) {
                        foreach ($detalles as $index => $objCursoDetalleSolicitud) {
                            //$objCursoDetalleSolicitud->convalida = $this->convalidacionService->getTextConvalida($objCursoDetalleSolicitud, $objSolicitud->int_statusid);
                            $arrayResultEstado = $this->convalidacionService->getTextConvalidaNew($objCursoDetalleSolicitud, $objSolicitud->int_statusid, $objSolicitud);
                            $objCursoDetalleSolicitud->convalida = $arrayResultEstado['html_status'];
                            $objCursoDetalleSolicitud->report_solicitud_status = $arrayResultEstado['chr_status'];
                            $objCursoDetalleSolicitud->report_solicitud_asignado_a = $arrayResultEstado['chr_asignadoa'];
                            $objCursoDetalleSolicitud->report_responsable_regla = $this->getNameResponsableRegla($objCursoDetalleSolicitud->int_cursoid_local);
                            
                            $objCursoDetalleSolicitud->sustento = $this->convalidacionService->getTextSustento($objCursoDetalleSolicitud);
                            $objCursoDetalleSolicitud->condicion = ($objCursoDetalleSolicitud->is_valid == 0) ? 'No aplica' : '-';
                             $returnUsuario = $this->convalidacionService->getUsuario($objCursoDetalleSolicitud, $objSolicitud->int_statusid);
                             $objCursoDetalleSolicitud->usuario = $returnUsuario['usuario'];
                             $objCursoDetalleSolicitud->date_convalidacion = $returnUsuario['date'];
                            $detalles[$index] = $objCursoDetalleSolicitud;
                        }
                    }
                    break;
                case \convalidacion\Model\ConvalidacionModel::STATUS_SOLICITUD_FINALIZADA:
                case \convalidacion\Model\ConvalidacionModel::STATUS_SOLICITUD_FACTURABLE:
                case \convalidacion\Model\ConvalidacionModel::STATUS_SOLICITUD_PENDIENTE_FACTURABLE:
                    if (is_array($detalles) && count($detalles) > 0) {
                        foreach ($detalles as $index => $objCursoDetalleSolicitud) {
                            //$objCursoDetalleSolicitud->convalida = $this->convalidacionService->getTextConvalida($objCursoDetalleSolicitud, $objSolicitud->int_statusid);
                            $arrayResultEstado = $this->convalidacionService->getTextConvalidaNew($objCursoDetalleSolicitud, $objSolicitud->int_statusid, $objSolicitud);
                            $objCursoDetalleSolicitud->convalida = $arrayResultEstado['html_status'];
                            $objCursoDetalleSolicitud->report_solicitud_status = $arrayResultEstado['chr_status'];
                            $objCursoDetalleSolicitud->report_solicitud_asignado_a = $arrayResultEstado['chr_asignadoa'];
                            $objCursoDetalleSolicitud->report_responsable_regla = $this->getNameResponsableRegla($objCursoDetalleSolicitud->int_cursoid_local);
                            
                            $objCursoDetalleSolicitud->sustento = $this->convalidacionService->getTextSustento($objCursoDetalleSolicitud);
                            $arrayResultCondicion = $this->convalidacionService->getCondicion($objCursoDetalleSolicitud);
                            $objCursoDetalleSolicitud->condicion = $arrayResultCondicion['condicion'];
                             $returnUsuario = $this->convalidacionService->getUsuario($objCursoDetalleSolicitud, $objSolicitud->int_statusid);
                             $objCursoDetalleSolicitud->usuario = $returnUsuario['usuario'];
                             $objCursoDetalleSolicitud->date_convalidacion = $returnUsuario['date'];
                            $detalles[$index] = $objCursoDetalleSolicitud;
                        }
                    }
                    break;
                case \convalidacion\Model\ConvalidacionModel::STATUS_SOLICITUD_ENVIADO_DIRECTOR_CARRERA:
                case \convalidacion\Model\ConvalidacionModel::STATUS_SOLICITUD_ENVIADO_DIRECCION_ACADEMICA:
                    if (is_array($detalles) && count($detalles) > 0) {
                        foreach ($detalles as $index => $objCursoDetalleSolicitud) {
                            //$objCursoDetalleSolicitud->convalida = $this->convalidacionService->getTextConvalida($objCursoDetalleSolicitud, $objSolicitud->int_statusid); //($objCursoDetalleSolicitud->is_valid == 0) ? 'No aplica' : $this->getStatusName($objCursoDetalleSolicitud->int_statusid);
                            $arrayResultEstado = $this->convalidacionService->getTextConvalidaNew($objCursoDetalleSolicitud, $objSolicitud->int_statusid, $objSolicitud);
                            $objCursoDetalleSolicitud->convalida = $arrayResultEstado['html_status'];
                            $objCursoDetalleSolicitud->report_solicitud_status = $arrayResultEstado['chr_status'];
                            $objCursoDetalleSolicitud->report_solicitud_asignado_a = $arrayResultEstado['chr_asignadoa'];
                            $objCursoDetalleSolicitud->report_responsable_regla = $this->getNameResponsableRegla($objCursoDetalleSolicitud->int_cursoid_local);
                            
                            $objCursoDetalleSolicitud->sustento = $this->convalidacionService->getTextSustento($objCursoDetalleSolicitud); //($objCursoDetalleSolicitud->is_valid == 0) ? 'No aplica' : $this->getSustento($objCursoDetalleSolicitud);
                            $arrayResultCondicion = $this->convalidacionService->getCondicion($objCursoDetalleSolicitud);
                            $objCursoDetalleSolicitud->condicion = $arrayResultCondicion['condicion'];
                             $returnUsuario = $this->convalidacionService->getUsuario($objCursoDetalleSolicitud, $objSolicitud->int_statusid);
                             $objCursoDetalleSolicitud->usuario = $returnUsuario['usuario'];
                             if($objCursoDetalleSolicitud->is_valid==0){
                                 $objCursoDetalleSolicitud->usuario = $this->utilService->getNameUsuario($objCursoDetalleSolicitud->int_user_validated);
                             }
                             $objCursoDetalleSolicitud->date_convalidacion = $returnUsuario['date'];
                            $detalles[$index] = $objCursoDetalleSolicitud;
                        }
                    }
                    break;
            }
            $objSolicitud->detalle = $detalles;
            $data['objSolicitud'] = $objSolicitud;
        } else {
            $error = 1;
            $message = 'No se encontró registro con el ID: ' . $id;
        }
        $returnValue['data'] = $data;
        $returnValue['error'] = $error;
        $returnValue['message'] = $message;
        return $returnValue;
    }
    
    public function getNameResponsableRegla($cursoid){
        $returnValue = '';
        $this->utilService = new UtilService();
        if(strlen(trim($cursoid))>0 && $cursoid >0){
            //Obtenemos los directores por curso
            $objCurso = ReporteModel::getCursoById($cursoid);
            if(is_object($objCurso)){
                $returnValue = $this->utilService->getNameDirectorByDireccion($objCurso->int_direccionid);
            }
        }
        return $returnValue;
    }
    
    public function getSolicitudToSeguimiento($objSolicitud) {
        $array_detalle_solicitud = [];
        $array_id_convalidaion_unique = [];
        //obtenemos los datos de las tablas de la BD, omitimos el algoritmo de búsqueda
        $objSolicitud->detalle = \convalidacion\Model\ConvalidacionModel::getSolicitudDetalleBySolicitudIdForConvalidacion($objSolicitud->id);
        //iteramos los cursos de la solicitud y buscamos la relación con las agrupaciones
        if (is_array($objSolicitud->detalle) && count($objSolicitud->detalle) > 0) {
            foreach ($objSolicitud->detalle as $indice => $objSolicitudDetalle) {
                //Obtenemos los datos de las tablas de agrupaciones de la ficha
                $array_resultado = $this->getConvalidacionesTemporales($objSolicitud, $objSolicitudDetalle);
                if (is_array($array_resultado) && count($array_resultado) > 0) {
                    foreach ($array_resultado as $puntero => $objSol) {
                        if ($objSol->has_convalidacion) {
                            if (!in_array($objSol->int_convalidacionid, $array_id_convalidaion_unique)) {
                                array_push($array_id_convalidaion_unique, $objSol->int_convalidacionid);
                                array_push($array_detalle_solicitud, $objSol);
                            }
                        } else {
                            array_push($array_detalle_solicitud, $objSol);
                        }
                    }
                }
            }
        }
        $objSolicitud->detalle = $array_detalle_solicitud;
        return $objSolicitud;
    }

    public function getConvalidacionesTemporales($objSolicitud, $objSolicitudDetalle) {
        $returnValue = [];
        //obtenemos el primer objeto de la relacion detalle solicitud y agrupacion
        $arrayAgrupaciones = \convalidacion\Model\ConvalidacionModel::getRelacionSolicitudAgrupacion($objSolicitudDetalle->id);   
//        print_r("\n--------------------------------------------------------------------------------------------\n");
//        print_r($arrayAgrupaciones);
        if (is_array($arrayAgrupaciones) && count($arrayAgrupaciones) > 0) {
            foreach ($arrayAgrupaciones as $index => $objIdAgrupacion) {
                $objAgrupacion = \convalidacion\Model\ConvalidacionModel::getAgrupacionByIdWithMalla($objIdAgrupacion->int_agrupacionid);
                //$objetoSolicitudDetalle1 = \convalidacion\Model\ConvalidacionModel::getSolicitudDetalleById($objSolicitudDetalle->id);
                $objetoSolicitudDetalle = \convalidacion\Model\ConvalidacionModel::getSolicitudDetalleById($objSolicitudDetalle->id);
                //obtenemos los detalles de la solicitud de cada convalidacion
                $arrayIdSolicitudDetalle = \convalidacion\Model\ConvalidacionModel::getIdSolicitudDetalleByAgrupacionId($objIdAgrupacion->int_agrupacionid);
                //iteramos el resultado para crear nuevas estructuras
                $convalidacionid = 0;
                $agrupacionid = 0;
                $status = \convalidacion\Model\ConvalidacionModel::STATUS_FICHA_CONVALIDACION_SIN_REGISTRO;
                $cursos = [];
                $codigos = [];
                $nota = [];
                $credito = [];
                $periodo = [];
                $silabos = [];
                if (is_array($arrayIdSolicitudDetalle) && count($arrayIdSolicitudDetalle) > 0) {
                    foreach ($arrayIdSolicitudDetalle as $puntero => $objAgrupacionConvalidacion) {
                        $convalidacionid = $objAgrupacionConvalidacion->int_convalidacionid;
                        $agrupacionid = $objAgrupacionConvalidacion->int_agrupacionid;
                        $status = ($objAgrupacionConvalidacion->int_statusid == \convalidacion\Model\ConvalidacionModel::STATUS_CONVALIDACION_APROBADA) ? \convalidacion\Model\ConvalidacionModel::STATUS_FICHA_CONVALIDACION_APROBADA : \convalidacion\Model\ConvalidacionModel::STATUS_FICHA_CONVALIDACION_RECHAZADA;
                        $oSolicitudDetalle = \convalidacion\Model\ConvalidacionModel::getSolicitudDetalleById($objAgrupacionConvalidacion->int_solicitud_detalleid);
                        array_push($cursos, $oSolicitudDetalle->chr_curso_name);
                        array_push($codigos, $oSolicitudDetalle->cod_curso);
                        array_push($nota, $oSolicitudDetalle->dec_grade);
                        array_push($credito, $oSolicitudDetalle->int_credito);
                        array_push($periodo, $oSolicitudDetalle->chr_periodo);
                        array_push($silabos, $oSolicitudDetalle->id);
                    }
                }
                $oStatus = \convalidacion\Model\ConvalidacionModel::getStatusById($status);
                $objetoSolicitudDetalle->silabos = implode(',', $silabos);
                $objetoSolicitudDetalle->cursos = implode(',', $cursos);
                $objetoSolicitudDetalle->cod_curso = implode(',', $codigos);
                $objetoSolicitudDetalle->dec_grade = implode('-', $nota);
                $objetoSolicitudDetalle->int_credito = implode('-', $credito);
                $objetoSolicitudDetalle->chr_periodo = implode(',', $periodo);
                $objetoSolicitudDetalle->int_convalidacionid = $convalidacionid;
                $objetoSolicitudDetalle->int_agrupacionid = $agrupacionid;
                $objetoSolicitudDetalle->int_statusid = $oStatus->id;
                $objetoSolicitudDetalle->class = ($oStatus->id == \convalidacion\Model\ConvalidacionModel::STATUS_FICHA_CONVALIDACION_APROBADA) ? 'table-success' : 'table-danger';
                $objetoSolicitudDetalle->chr_status = $oStatus->chr_name;
                $objetoSolicitudDetalle->has_convalidacion = 1;
                $objetoSolicitudDetalle->asignatura_local = $this->getCursosLocalesByAgrupacionId($objAgrupacion->id);
                //Obtenemos el objeto de curso UPC, por regla general sólo es un solo curso
                $objCursoUPC = $this->getCursoLocalByAgrupacionId($objAgrupacion->id);
//                if(!is_object($objCursoUPC)){
//                    print_r($objAgrupacion);
//                    print_r($objSolicitudDetalle);
//                    die();
//                }
                $objCursoEquivalencia = \convalidacion\Model\ConvalidacionModel::getCursoSimpleById($objCursoUPC->chr_code_equivalente);
                $objmallaUPC= \convalidacion\Model\CursosModel::getMallasByCursoIdByCarreraId($objCursoUPC->id,$objSolicitud->int_carreraid_to, $objSolicitud->int_mallaid);
                $objmallaUPCEquivalente='';
                if(!is_object($objCursoEquivalencia)){
                    $objCursoEquivalencia = new stdClass();
                    $objCursoEquivalencia->id = 0;
                    $objCursoEquivalencia->chr_name = '';
                    $objCursoEquivalencia->chr_code = '';
                    $objmallaUPCEquivalente= \convalidacion\Model\CursosModel::getMallasByCursoIdByCarreraId($objCursoEquivalencia->id,$objSolicitud->int_carreraid_to, $objSolicitud->int_mallaid);
                }
                $objetoSolicitudDetalle->int_cursoid_local = $objCursoUPC->id;
                $objetoSolicitudDetalle->chr_curso_local = $objCursoUPC->chr_name;
                $objetoSolicitudDetalle->chr_code_curso_local = $objCursoUPC->chr_code;
                $objetoSolicitudDetalle->chr_malla_curso_local = $objmallaUPC;
                $objetoSolicitudDetalle->int_cursoid_local_equivalente = $objCursoEquivalencia->id;
                $objetoSolicitudDetalle->chr_curso_local_equivalente = $objCursoEquivalencia->chr_name;
                $objetoSolicitudDetalle->chr_code_curso_local_equivalente = $objCursoEquivalencia->chr_code;
                $objetoSolicitudDetalle->chr_malla_curso_local_equivalente = $objmallaUPCEquivalente;
                if(is_object($objAgrupacion)){
                    $objetoSolicitudDetalle->txt_validated = $objAgrupacion->txt_sustento;
                    //chancamos la validación de la agrupación al detalle de solcitud
                    $objetoSolicitudDetalle->is_valid = $objAgrupacion->is_valid;
                    $objetoSolicitudDetalle->date_timevalidated = $objAgrupacion->date_timevalidated;
                    $objetoSolicitudDetalle->int_user_validated = $objAgrupacion->int_user_validated;
                    $mallas_director = '';
                    if(strlen(trim($objAgrupacion->chr_code_malla_director))>0){
                        $mallas_director = '['.$objAgrupacion->chr_code_malla_director.'] '.$objAgrupacion->chr_malla_director;
                    }
                    $objetoSolicitudDetalle->chr_malla_director = $mallas_director;
                    //Verificamos y validamos los estados con condiciones
                    if(in_array($objAgrupacion->int_statusid_condicion, [\convalidacion\Model\ConvalidacionModel::STATUS_CONDICION_APROBADA, \convalidacion\Model\ConvalidacionModel::STATUS_CONDICION_RECHAZADA])){
                        $objetoSolicitudDetalle->class = ($objAgrupacion->int_statusid_condicion == \convalidacion\Model\ConvalidacionModel::STATUS_CONDICION_APROBADA) ? 'table-success' : 'table-danger';
                        $oStatusTemp = \convalidacion\Model\ConvalidacionModel::getStatusById($objAgrupacion->int_statusid_condicion);
                        $objetoSolicitudDetalle->int_statusid = $oStatusTemp->id;
                        $objetoSolicitudDetalle->chr_status = $oStatusTemp->chr_name;
                        $objetoSolicitudDetalle->txt_validated = \convalidacion\Model\ConvalidacionModel::getFeedbackCondicionByAgrupacionId($objAgrupacion->id);
                    }
                }
                $objetoSolicitudDetalle->agrupacion = $arrayIdSolicitudDetalle;
                $objetoSolicitudDetalle->estados_curso = $objSolicitudDetalle->estados_curso;
                $objetoSolicitudDetalle->estados_solicitud = $objSolicitudDetalle->estados_solicitud;
                $objetoSolicitudDetalle->condiciones = \convalidacion\Model\ConvalidacionModel::getAllCondicionesBySolicitudDetalleIdTemp($objSolicitudDetalle->id, $convalidacionid);                
                array_push($returnValue, $objetoSolicitudDetalle);
                //}
            }
        } else {
            $objetoSolicitudDetalle = \convalidacion\Model\ConvalidacionModel::getSolicitudDetalleById($objSolicitudDetalle->id);
            //al no encontrarse convalidaciones en automático, retornamos el detalle de la solicitud
            $objetoSolicitudDetalle->cursos = $objSolicitudDetalle->chr_curso_name;
            $objetoSolicitudDetalle->has_convalidacion = 0;
            $objetoSolicitudDetalle->int_convalidacionid = 0;
            $objetoSolicitudDetalle->int_agrupacionid = 0;
            $objetoSolicitudDetalle->asignatura_local = '-';
            $objetoSolicitudDetalle->int_cursoid_local = '';
            $objetoSolicitudDetalle->chr_curso_local = '';
            $objetoSolicitudDetalle->chr_code_curso_local = '';
            $objetoSolicitudDetalle->chr_malla_curso_local = '';
            $objetoSolicitudDetalle->chr_malla_curso_local_equivalente = '';
            $objetoSolicitudDetalle->chr_malla_director = '';
            $objetoSolicitudDetalle->int_cursoid_local_equivalente = '';
            $objetoSolicitudDetalle->chr_curso_local_equivalente = '';
            $objetoSolicitudDetalle->chr_code_curso_local_equivalente = '';
            $objetoSolicitudDetalle->class = 'table-default';
            $objetoSolicitudDetalle->agrupacion = [];
            $objetoSolicitudDetalle->silabos = $objetoSolicitudDetalle->id;
            $objetoSolicitudDetalle->estados_curso = $objSolicitudDetalle->estados_curso;
            $objetoSolicitudDetalle->estados_solicitud = $objSolicitudDetalle->estados_solicitud;
            $objetoSolicitudDetalle->condiciones = [];            
            array_push($returnValue, $objetoSolicitudDetalle);
        }   
        return $returnValue;
    }
    
    public function getTextConvalida2($objCursoDetalleSolicitud, $statusSolicitud=0) {
//        print_object($objCursoDetalleSolicitud);
//        print_object($statusSolicitud);die();
        $returnValue = 'No aplica';
        $this->utilService = new UtilService();
        if (is_object($objCursoDetalleSolicitud)) {
            //Obtenemos el objeto de la solicitud
            $objSolicitud = $this->getSolicitudSingleById($objCursoDetalleSolicitud->int_solicitudid);
            switch ($objCursoDetalleSolicitud->is_valid) {
                case \convalidacion\Model\SolicitudModel::IS_VALID_ZERO:
                    break;
                case \convalidacion\Model\SolicitudModel::IS_VALID:
                    //mostramos el estado en la que está este detalle
                    switch ($objCursoDetalleSolicitud->int_statusid) {
                        case \convalidacion\Model\SolicitudModel::STATUS_SOLICITUD_DETALLE_SIN_REGISTRO:
                            switch($statusSolicitud){
                                case \convalidacion\Model\SolicitudModel::STATUS_SOLICITUD_INICIADA:
                                    $returnValue = 'Validado por admisión  - <small><i> ' . $this->utilService->timeago($objCursoDetalleSolicitud->date_timevalidated) . '</i></small>';
                                    break;
                                default:
                                    //obtenemos el nombre del director registrado en el grupo
                                      //buscamos si el detalle de la solicitud tiene un grupo
                                      $objGrupo = $this->getGrupoBySolicitudDetalleId($objCursoDetalleSolicitud->id);
                                      //$objGrupo = $this->getGrupoValidadoBySolicitudDetalleId($objCursoDetalleSolicitud->id);
                                      if (is_object($objGrupo)) {
                                          //$returnValue = 'Enviado al director ' . $this->utilService->getNameUsuario($objGrupo->int_user_assigned) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                          //validamos si está en estado nivel 3
                                          if($objGrupo->int_statusid  == ConvalidacionModel::STATUS_GRUPO_CURSOS_NIVEL_3){
                                              $returnValue = 'Enviado al director' . $this->utilService->getNameDecanoByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                          }elseif($objGrupo->int_statusid  == ConvalidacionModel::STATUS_GRUPO_NO_APLICA_REGLA){
                                              $returnValue = $this->getStatusName($objGrupo->int_statusid);
                                          }else{
                                              //Verificamos que el grupo esté enviado a una direccion
                                              if($objGrupo->int_direccionid>0){
                                                  $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByDireccion($objGrupo->int_direccionid) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                              }else{
                                                  $returnValue = 'Enviado al director  ' . $this->utilService->getNameDirectorByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                              }
                                          }
                                      } else {
                                          //buscamos en el detalle de la solicitud el director asignado
                                          //$returnValue = 'Enviado al director ' . $this->utilService->getNameUsuario($objCursoDetalleSolicitud->int_user_director_carrera) . ' - <small><i> ' . $this->utilService->timeago($objCursoDetalleSolicitud->date_timedirector_carrera) . '</i></small>';
                                          $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i> ' . $this->utilService->timeago($objCursoDetalleSolicitud->date_timedirector_carrera) . '</i></small>';
                                      }                                    
                                    break;
                            }
                            break;
                        case \convalidacion\Model\SolicitudModel::STATUS_SOLICITUD_DETALLE_APROBADA:
                            if($objCursoDetalleSolicitud->int_agrupacionid >0){
                                $objAgrupacion = ConvalidacionModel::getAgrupacionById($objCursoDetalleSolicitud->int_agrupacionid);
                                if(is_object($objAgrupacion)){
                                    $returnValue = $this->getStatusName($objAgrupacion->int_statusid);
                                }
                            }else{
                                $objGrupo = $this->getGrupoBySolicitudDetalleId($objCursoDetalleSolicitud->id);
                                if (is_object($objGrupo)) {
                                    //validamos si está en estado nivel 3
                                    if($objGrupo->int_statusid  == ConvalidacionModel::STATUS_GRUPO_CURSOS_NIVEL_3){
                                        $returnValue = 'Enviado al director ' . $this->utilService->getNameDecanoByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                    }else{
                                        //Verificamos que el grupo esté enviado a una direccion
                                        if($objGrupo->int_direccionid>0){
                                            $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByDireccion($objGrupo->int_direccionid) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                        }else{
                                            $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                        }
                                    }
                                } else {
                                    //buscamos en el detalle de la solicitud el director asignado
                                    $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i> ' . $this->utilService->timeago($objCursoDetalleSolicitud->date_timedirector_carrera) . '</i></small>';
                                }                                
                            }
                            break;
                        case \convalidacion\Model\SolicitudModel::STATUS_SOLICITUD_DETALLE_RECHAZADA:
                            if($objCursoDetalleSolicitud->int_agrupacionid >0){
                                $objAgrupacion = ConvalidacionModel::getAgrupacionById($objCursoDetalleSolicitud->int_agrupacionid);
                                if(is_object($objAgrupacion)){
                                    $returnValue = $this->getStatusName($objAgrupacion->int_statusid);
                                }
                            }else{
                                $objGrupo = $this->getGrupoBySolicitudDetalleId($objCursoDetalleSolicitud->id);
                                if (is_object($objGrupo)) {
                                    //validamos si está en estado nivel 3
                                    if($objGrupo->int_statusid  == ConvalidacionModel::STATUS_GRUPO_CURSOS_NIVEL_3){
                                        $returnValue = 'Enviado al director ' . $this->utilService->getNameDecanoByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                    }else{
                                        //Verificamos que el grupo esté enviado a una direccion
                                        if($objGrupo->int_direccionid>0){
                                            $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByDireccion($objGrupo->int_direccionid) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                        }else{
                                            $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i>Hace ' . $this->utilService->timeago($objGrupo->date_timeassigned . '</i></small>');
                                        }
                                    }
                                } else {
                                    //buscamos en el detalle de la solicitud el director asignado
                                    $returnValue = 'Enviado al director ' . $this->utilService->getNameDirectorByCarrera($objSolicitud->int_carreraid_to) . ' - <small><i> ' . $this->utilService->timeago($objCursoDetalleSolicitud->date_timedirector_carrera) . '</i></small>';
                                }
                            }
                            break;
                        case ConvalidacionModel::STATUS_CONDICION_APROBADA:
                        case ConvalidacionModel::STATUS_CONDICION_RECHAZADA:
                            $returnValue = $objCursoDetalleSolicitud->chr_status;
                            break;
                    }
                    break;
                case \convalidacion\Model\SolicitudModel::IS_INVALID:
                    $returnValue = 'Rechazado por admisión' . ' - <i><small>' . $this->utilService->timeago($objCursoDetalleSolicitud->date_timevalidated) . '</small></i>';
                    break;
            }
        }
        return $returnValue;
    }    
    
    public function getCursoLocalByAgrupacionId($agrupacionid) {
        $returnValue = NULL;
        $array_cursos = \convalidacion\Model\ConvalidacionModel::getDetalleAgrupacionById($agrupacionid, 0);
        if (is_array($array_cursos) && count($array_cursos) > 0) {
            foreach ($array_cursos as $index => $objCursoDetalleAgrupacion) {
                $returnValue = \convalidacion\Model\CursosModel::getCursoSingleById($objCursoDetalleAgrupacion->int_cursoid);
                break;
            }
        }
        if(is_null($returnValue)){
            $objCursoUPC = new \stdClass();
            $objCursoUPC->id = 0;
            $objCursoUPC->chr_name = '';
            $objCursoUPC->chr_code = '';
            $objCursoUPC->chr_code_equivalente = 0;
            $returnValue = $objCursoUPC;
        }
        return $returnValue;
    }    

    public function getCursosLocalesByAgrupacionId($agrupacionid) {
        $result = '';
        $returnValue = [];
        $array_cursos = \convalidacion\Model\ConvalidacionModel::getDetalleAgrupacionById($agrupacionid, 0);
        if (is_array($array_cursos) && count($array_cursos) > 0) {
            foreach ($array_cursos as $index => $objCurso) {
                if (!in_array($objCurso->chr_name, $returnValue)) {
                    array_push($returnValue, '[' . $objCurso->chr_code . '] ' . $objCurso->chr_name);
                }
            }
        }
        if (is_array($returnValue) && count($returnValue) > 0) {
            $result = implode(',', $returnValue);
        }
        return $result;
    }    

    /**
     * Obtenemos las solicitudes de facturación en un intervalo de tiempo
     * @param int $desde timestamp
     * @param int $hasta timestamp
     * @param int $status
     * @return array
     */
    public function getSolicitudesFacturacion($desde, $hasta, $status=0){
        $this->utilService = new UtilService();        
        //obtenemos las solicitudes
        $solicitudes = ReporteModel::getSolicitudesByFechas($desde, $hasta, $status);
        if(is_array($solicitudes) && count($solicitudes)>0){
            foreach($solicitudes as $index=>$objSolicitud){
                $objSolicitud->chr_ciclo = $this->utilService->getCicloByFecha($objSolicitud->date_timecreated);
                $objSolicitud->num_cursos_convalida = $this->getCountCursosConvalidaBySolicitud($objSolicitud->id);
                $solicitudes[$index] = $objSolicitud;
            }
        }
        return $solicitudes;
    }
    
    public function getCountCursosConvalidaBySolicitud($solicitudid){
        $returnValue = 0;
        //obtenemos los id de los detalles de la solicitud
        $array_id_detalle = $this->getIdsDetalleSolicitudBySolicitudId($solicitudid);
        if(is_array($array_id_detalle) && count($array_id_detalle)>0){
            //Buscamos el detalle en las convas
            //obtenemos los id de agrupaciones aprobadas
            $agrupaciones_aprobadas = $this->getAgrupacionesAprobadasBySolicitudDetalleIds($array_id_detalle);
            if(is_array($agrupaciones_aprobadas) && count($agrupaciones_aprobadas)){
                $returnValue = count($agrupaciones_aprobadas);
            }
        }
        return $returnValue;
    }
    
    public function getAgrupacionesAprobadasBySolicitudDetalleIds($array_id_detalle){
        $returnValue = [];
        $array_id_agrupaciones = [];
        $array_convas = ReportesModel::getConvasBySolicitudDetalle($array_id_detalle);
        if(is_array($array_convas) && count($array_convas)>0){
            foreach($array_convas as $index=>$objConva){
                if(!in_array($objConva->int_agrupacionid, $array_id_agrupaciones)){
                    array_push($array_id_agrupaciones, $objConva->int_agrupacionid);
                }
            }
        }
        if(is_array($array_id_agrupaciones) && count($array_id_agrupaciones)>0){
            //obtenemos la cantidad de registros aprobados
            $returnValue = ReportesModel::getAgrupacionesAprobadasByIds($array_id_agrupaciones);
        }
        return $returnValue;
    }
    
    public function getIdsDetalleSolicitudBySolicitudId($solicitudid){
        $returnValue = [];
        $array_solicitudes = ReportesModel::getDetalleSolicitudBySolicitudId($solicitudid);
        if(is_array($array_solicitudes) && count($array_solicitudes)>0){
            foreach($array_solicitudes as $index=>$objSolicitudDetalle){
                if(!in_array($objSolicitudDetalle->id, $returnValue)){
                    array_push($returnValue, $objSolicitudDetalle->id);
                }
            }
        }
        return $returnValue;
    }
    
    /**
     * Listamos los cursos de solicitudes en un rango de fechas
     * @param int $desde timestamp
     * @param int $hasta timestamp
     * @param int $status
     * @return array
     */
    public function getCursosOfSolicitudesByFecha($desde, $hasta, $status=0, $modalidadid=0, $cicloincorporacionid=0){
        $returnValue = [];
        $array_id_curso = [];
        $array_id_solicitud = [];
        $array_id_agrupaciones = [];
        //$solicitudes = ReportesModel::getSolicitudesByFechas($desde, $hasta, $status);
        $solicitudes = ReportesModel::getSolicitudesByFechas($desde, $hasta, $status, $modalidadid, $cicloincorporacionid);
        if(is_array($solicitudes) && count($solicitudes)>0){
            foreach($solicitudes as $index=>$objSolicitud){
                if(!in_array($objSolicitud->id, $array_id_solicitud)){
                    array_push($array_id_solicitud, $objSolicitud->id);
                }
            }
        }
        if(is_array($array_id_solicitud) && count($array_id_solicitud)>0){
            $array_id_solicitd_detalle = $this->getIdDetalleSolicitud($array_id_solicitud);
            if(is_array($array_id_solicitd_detalle) && count($array_id_solicitd_detalle)>0){
                //obtenemos todos los ID's de las agrupaciones aprobadas
                $array_id_agrupaciones = $this->getIdAgrupacionBySolicitudDetalle($array_id_solicitd_detalle);
                //purgamos solo los ID's aprobados
                $array_id_agrupaciones = $this->purgeAgrupacionesAprobadas($array_id_agrupaciones);
                //obtenemos los Id de los cursos que pertenecen a estás agrupaciones aprobadas
                if(is_array($array_id_agrupaciones) && count($array_id_agrupaciones)>0){
                    $array_id_curso = $this->getCursosByAgrupación($array_id_agrupaciones);
                }
            }
        }
        if(is_array($array_id_curso) && count($array_id_curso)>0){
            $returnValue = ReportesModel::getCursosByIds($array_id_curso);
            //De la lista de estos cursos agregamos los alumnos que están relacionados
            if(is_array($returnValue) && count($returnValue)>0){
                foreach($returnValue as $indice=>$objCurso){
                    $objCurso->alumnos = $this->getAlumnosByCursoBySolicitudes($objCurso, $array_id_agrupaciones);
                    $returnValue[$indice] = $objCurso;
                }
            }
        }
        return $returnValue;
    }
    
    public function getAlumnosByCursoBySolicitudes($objCurso, $array_id_agrupaciones){
        $returnValue = [];
        //buscamos en el detalle de las solicitudes este curso y obtenemos los alumnos
        //Primero obtenemos las solicitudes que tienen este curso
        $array_id_solicitudes_detalle = $this->getIdSolicitudesDetalleByCurso($objCurso->id, $array_id_agrupaciones);
        
        //Segundo listamos solo los cod alumnos y las retornamos en lista
        $array_id_solicitud = $this->getIdSolicitudesByArrayIdSolicituDetalle($array_id_solicitudes_detalle);
        
        if(is_array($array_id_solicitud) && count($array_id_solicitud)>0){
            $solicitudes = ReportesModel::getSolicitudesByIds($array_id_solicitud);
            if(is_array($solicitudes) && count($solicitudes)>0){
                foreach($solicitudes as $index=>$objSolicitud){
                    if(!in_array($objSolicitud->chr_code_student, $returnValue)){
                        array_push($returnValue,$objSolicitud->chr_code_student);
                    }
                }
            }
        }
        return $returnValue;
    }
    
    public function getIdSolicitudesByArrayIdSolicituDetalle($array_id_solicitudes_detalle){
        $returnValue = [];
        $solicitud_detalle = ReportesModel::getIdSolicitudesByArrayIdSolicituDetalle($array_id_solicitudes_detalle);
        if(is_array($solicitud_detalle) && count($solicitud_detalle)>0){
            foreach($solicitud_detalle as $index=>$objSolicitudDetalle){
                if(!in_array($objSolicitudDetalle->int_solicitudid, $returnValue)){
                    array_push($returnValue, $objSolicitudDetalle->int_solicitudid);
                }
            }
        }
        return $returnValue;
    }
    
    public function getIdSolicitudesDetalleByCurso($int_cursoid, $array_id_agrupaciones){
        $returnValue = [];
        $array_id_agrupaciones_curso = [];
        //obtenemos todos los registros de todo el detalle de estás agrupaciones
        $array_agrupaciones_detalle = ReportesModel::getIdAgrupacionesByCurso($int_cursoid, $array_id_agrupaciones);
        //itermos para obtenes la nueva lista actualiza de agrupaciones
        if(is_array($array_agrupaciones_detalle) && count($array_agrupaciones_detalle)>0){
            foreach($array_agrupaciones_detalle as $index=>$objAgrupacionDetalle){
                if(!in_array($objAgrupacionDetalle->int_agrupacionid, $array_id_agrupaciones_curso)){
                    array_push($array_id_agrupaciones_curso, $objAgrupacionDetalle->int_agrupacionid);
                }
            }
        }
        //obtenemos los Id de las solicitudes detalle
        if(is_array($array_id_agrupaciones_curso) && count($array_id_agrupaciones_curso)>0){
            $array_conva = ReportesModel::getConvasByAgrupaciones($array_id_agrupaciones_curso);
            if(is_array($array_conva) && count($array_conva)){
                foreach($array_conva as $puntero=>$objConva){
                    if(!in_array($objConva->int_solicitud_detalleid, $returnValue)){
                        array_push($returnValue, $objConva->int_solicitud_detalleid);
                    }
                }
            }
        }
        return $returnValue;
    }
    
    public function getCursosByAgrupación($array_id_agrupaciones){
        $returnValue = [];
        $detalle_agrupaciones = ReportesModel::getCursosByAgrupación($array_id_agrupaciones);
        if(is_array($detalle_agrupaciones) && count($detalle_agrupaciones)>0){
            foreach($detalle_agrupaciones as $index=>$objAgrupacionDetalle){
                if(!in_array($objAgrupacionDetalle->int_cursoid, $returnValue)){
                    array_push($returnValue, $objAgrupacionDetalle->int_cursoid);
                }
            }
        }
        return $returnValue;
    }
    
    public function purgeAgrupacionesAprobadas($array_id_agrupaciones){
        $returnValue = [];
        $agrupaciones = ReportesModel::getAgrupacionesAprobadasByIds($array_id_agrupaciones);
        if(is_array($agrupaciones) && count($agrupaciones)>0){
            foreach($agrupaciones as $index=>$objAgrupacion){
                if(!in_array($objAgrupacion->id, $returnValue)){
                    array_push($returnValue, $objAgrupacion->id);
                }
            }
        }
        return $returnValue;
    }
    
    public function getIdAgrupacionBySolicitudDetalle($array_id_solicitd_detalle){
        $returnValue = [];
        $detalle_conva = ReportesModel::getIdAgrupacionesConva($array_id_solicitd_detalle);
        if(is_array($detalle_conva) && count($detalle_conva)>0){
            foreach($detalle_conva as $index=>$objConva){
                if(!in_array($objConva->int_agrupacionid, $returnValue)){
                    array_push($returnValue, $objConva->int_agrupacionid);
                }
            }
        }
        return $returnValue;
    }
    
    public function getIdDetalleSolicitud($array_id_solicitud=[]){
        $returnValue = [];
        $detalle_solicitud =  ReportesModel::getIdDetalleSolicitud($array_id_solicitud);
        if(is_array($detalle_solicitud) && count($detalle_solicitud)>0){
            foreach($detalle_solicitud as $index=>$objDetalleSolicitud){
                if(!in_array($objDetalleSolicitud->id, $returnValue)){
                    array_push($returnValue, $objDetalleSolicitud->id);
                }
            }
        }
        return $returnValue;
    }
    
    public function getTopAdmision(){
        return ReporteModel::getTopAdmision();
    }
    
    public function getTopAlumnos(){
        return ReporteModel::getTopAlumnos();
    }
    
    public function getTopProcedencias(){
        return ReporteModel::getTopProcedencias();
    }
    
    public function getCursosNuevos($desde, $hasta, $institucionid, $roleid){
        return ReportesModel::getCursosNuevos($desde, $hasta, $institucionid, $roleid);
    }


    private function vd($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }
    
}