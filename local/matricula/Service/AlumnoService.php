<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Service;

use matricula\Core\Template;
use matricula\Model\AlumnoModel;
use context_system;
use moodle_url;
use stdClass;
use matricula\Model\UtilModel;

class AlumnoService extends Template {

    private $valid_exts = ['pdf', 'PDF'];
    private $max_size = 200000 * 1024;
    private $path_certificado;
    private $utilService;

    public function __construct() {
        $this->valid_exts = ['pdf', 'PDF'];
    }

   public static function getAlumnosAll() {
        return AlumnoModel::getAlumnosAll();;
    }
    
    
    public static function getAlumnosAllCursoid($id) {
        return AlumnoModel::getAlumnosAllCursoid($id);;
    }
    
    public static function getAlumnosAllNoOcupados() {
        return AlumnoModel::getAlumnosAllNoOcupados();;
    }
    
    public static function getCarrerasAll() {
        return AlumnoModel::getCarrerasAll();;
    }
    
    
    public static function getCiclosAll() {
        return AlumnoModel::getCiclosAll();;
    }
    
    public static function getAlumnoById($id) {
        return AlumnoModel::getAlumnoById($id);;
    }
    
    public static function getProductoById($id) {
        return AlumnoModel::getProductoById($id);;
    }
    
    
    public static function getAlumnoByDNI($dni) {
        return AlumnoModel::getAlumnoByDNI($dni);;
    }
    
    public static function getAlumnoBycode($dni) {
        return AlumnoModel::getAlumnoBycode($dni);;
    }
    
    public static function getAlumnoByCorreo($dni) {
        return AlumnoModel::getAlumnoByCorreo($dni);;
    }
    
    
    public static function getAlumnoByCelular($dni) {
        return AlumnoModel::getAlumnoByCelular($dni);;
    }
    
    public static function eliminarAlumno($id) {
        $objSolicitudBean = new \stdClass();
        $objSolicitudBean->id = $id;
        $objSolicitudBean->is_active = 0;
        $objSolicitudBean->is_deleted = 1;
        $objSolicitudBean->date_timemodified = time();
        return AlumnoModel::updateProducto($objSolicitudBean);;
    }
    
    public static function getMatriculaidByAlumnoid($id) {
        return AlumnoModel::getMatriculaidByAlumnoid($id);;
    }
    
     public function GuardarAlumno($inputs) {
        $id = $inputs->get('alumnoid');
        $name = $inputs->get('inputTxtName');
        $tipo = $inputs->get('TipoProducto');
        $precio = $inputs->get('Precio');
        global $USER;
        $obj = new \stdClass();
        //id
        $obj->id= $id;
        //nombre
        $obj->chr_name = $name;
        $obj->chr_tipo = $tipo;
        $obj->int_precio = $precio;
        
        //activo
        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        if ($id > 0) {
            //fecha modificacion registro
            $obj->date_timemodified = time();
            $returnValue= AlumnoModel::updateProducto($obj);
        
        }else{
            //fecha creacion registro
        $obj->date_timecreated = time();
        $returnValue = AlumnoModel::saveAlumno($obj);
        }
        

        $returnValue = $id;
        return $returnValue;
    }
    
    
     public function getUriEditAlumno($alumnoid) {
        return $this->routes()->generate('alumnos_editar', ['alumnoid' => $alumnoid]);
    }

}
