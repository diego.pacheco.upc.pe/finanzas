<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace matricula\Service;

use matricula\Core\Template;
use matricula\Model\CursoModel;
use context_system;
use moodle_url;
use stdClass;
use matricula\Model\UtilModel;

class CursoService extends Template {

    private $valid_exts = ['pdf', 'PDF'];
    private $max_size = 200000 * 1024;
    private $path_certificado;
    private $utilService;

    public function __construct() {
        $this->valid_exts = ['pdf', 'PDF'];
    }

   public static function getCursosAll() {
        return CursoModel::getCursosAll();;
    }
    
    
    public static function getCursosAllCiclos($ciclo) {
        return CursoModel::getCursosAllCiclos($ciclo);
    }
    public static function getCursoById($id) {
        return CursoModel::getCursoById($id);
    }
    
    public static function getCursosById($id) {
        return CursoModel::getCursosById($id);
    }
    
    public static function getCursoBySeccion($id) {
        return CursoModel::getCursoBySeccion($id);
    }
    
    public static function getCursoByCodigo($id) {
        return CursoModel::getCursoByCodigo($id);
    }
    
    
    public static function eliminarCurso($id) {
        $objSolicitudBean = new \stdClass();
        $objSolicitudBean->id = $id;
        $objSolicitudBean->is_active = 0;
        $objSolicitudBean->is_deleted = 1;
        $objSolicitudBean->date_timemodified = time();
        return CursoModel::updateCurso($objSolicitudBean);
    }
    
    
    public static function getCursoEstasiendoUsado($id) {
        return CursoModel::getCursoEstasiendoUsado($id);
    }
    
     public function GuardarCurso($inputs) {
        $id = $inputs->get('cursoid');
        global $USER;
        $obj = new \stdClass();
        //id
        $obj->id= $inputs->get('cursoid');
        //nombre
        $obj->chr_name = $inputs->get('inputTxtName');
        $obj->chr_name = strip_tags($obj->chr_name);
        $obj->chr_name = trim($obj->chr_name);
        //seccion
        $obj->chr_seccion = $inputs->get('inputTxtSeccion');
        $obj->chr_seccion = strip_tags($obj->chr_seccion);
        $obj->chr_seccion = trim($obj->chr_seccion);
        //codigo
        $obj->chr_code = $inputs->get('inputTxtCodigo');
        $obj->chr_code = strip_tags($obj->chr_code);
        $obj->chr_code = trim($obj->chr_code);
        //creditos
        $obj->int_creditos = $inputs->get('inputTxtCreditos');
        //carrera
        $obj->int_carreraid = $inputs->get('inputcarrera');
        $obj->chr_dia = $inputs->get('inputTxtDia');
        //ciclo
        $obj->int_ciclo = $inputs->get('inputTxtCiclo');
        //creditos
        $obj->int_cantidad_alumnos = $inputs->get('inputTxtcantidadAlumnos');
        //carrera
        $obj->chr_horainicio = $inputs->get('inputTxtInicio');
        //ciclo
        $obj->chr_horafin = $inputs->get('inputTxtFin');
        
        //activo
        $obj->is_active = 1;
        //eliminado
        $obj->is_deleted = 0;
        //creador
        //eliminado
        $obj->int_creatorid = $USER->id;
        if ($id > 0) {
            //fecha modificacion registro
            $obj->date_timemodified = time();
            $returnValue= CursoModel::updateCurso($obj);
        
        }else{
            //fecha creacion registro
        $obj->date_timecreated = time();
        $returnValue = CursoModel::saveCurso($obj);
        }
        

        $returnValue = $inputs->get('cursoid');
        return $returnValue;
    }
    
    
     public function getUriEditCurso($alumnoid) {
        return $this->routes()->generate('cursos_editar', ['cursoid' => $alumnoid]);
    }

}
